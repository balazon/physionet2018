import tqdm
import glob
import os

import h5py
import numpy as np
import scipy.io
import wfdb

from ecg import processECG
from eeg import processEEG
from annotations import processAnnotation, saveAnnotationAsBinaryFile
from check_features import displayFeatures

from multiprocessing import Pool

DATA_ROOT = "./data"
#DATA_ROOT = "d:\\Marci\\Prog\\Physionet2018\\"

def loadSingleData(path):
    basename = os.path.basename(os.path.dirname(path))
    header = wfdb.rdheader(path + "/" + basename)
    print("info: length of {}: {:.1f} hours".format(basename, header.sig_len/200.0/3600.0))
    
    features_dir = path.replace("training", "training_features").replace("test", "test_features")

    # Save length
    """
    f = open(features_dir + '/sample_count.txt', 'w')
    print(header.sig_len, file=f)
    f.close()
    """

    # signal = wfdb.rdrecord(path + "/" + basename)
    sig, fields = wfdb.rdsamp(path + "/" + basename)
    #signal.p_signal is the same as sig, signal has some additional data (things that can be found in fields, and maybe more)
    sig = sig.transpose()

    #arousal_ann = wfdb.rdann(path + "/" + basename, "arousal")
    # arousal_ann contains the sparse annotation
    #   arousal_ann.sample contains the timestamps for the annotations
    #   arousal_ann.subtype : the annotation value as a number 
    #   arousal_ann.aux_note : same as subtype but with a string ("W1", "N1", "R", "(resp_centralapnea", ...)


    os.makedirs(features_dir, exist_ok = True)
    
    # # Process ECG
    # ecg_index = fields['sig_name'].index('ECG')
    # processECG(sig[ecg_index], basename, features_dir)

    # # Process EEG
    # for i in range(6):
    #     processEEG(sig[i], basename, fields['sig_name'][i], features_dir)
    
    # Check features
    """
    features = glob.glob(os.path.join(path.replace("training/", "training_features/"), "*.bin"))
    if not features:
        print("No features found in " + DATA_ROOT)
    displayFeatures(features)
    """
    
    #Process Annotations
    with h5py.File(path + "/" + basename + "-arousal.mat", 'r') as f:
        # the binary data (and -1 when undefined)
        arousals = np.array(f["data/arousals"])
        assert arousals.shape[0] == sig.shape[1]
        #processAnnotation(f)
        saveAnnotationAsBinaryFile(path, basename, f)
    
def loadData():
    patients = glob.glob(os.path.join(DATA_ROOT, "training/*/"))
    if not patients:
        print("No data found in " + DATA_ROOT)

    parallel_processing = True
    if not parallel_processing:
        for p in tqdm.tqdm(patients): 
            loadSingleData(p) 

    if parallel_processing:
        with Pool(processes=8) as pool:
            # pool.map(loadSingleData, patients)
            max_ = len(patients)
            with tqdm.tqdm(total=max_) as pbar:
                for i, _ in tqdm.tqdm(enumerate(pool.imap_unordered(loadSingleData, patients))):
                    pbar.update()


if __name__ == "__main__":
    loadData()
