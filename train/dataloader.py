import os
import numpy as np
import itertools
import tensorflow as tf
import random
import h5py
import struct
import glob
#import wfdb

class dataloader:
    def __init__(self, train_sequences = None):
        self.time_slots = [-60, -30, -25, -20, -15, -10, -5, -3, -2, -1, 0, 1, 2, 3, 5, 10, 15, 20, 25, 30, 60]
        self.train_sequences = train_sequences  # Can be a pre-determinet set of patients (used during cross-validation)
        self.features_directory = 'training_normalized_features'
        
    def init_testing(self, feature_dir):
        self.existing_features = sorted([os.path.basename(f) for f in glob.glob(os.path.join(feature_dir, "*.bin"))])
        print("Found " + str(len(self.existing_features)) + " common features: " + str(self.existing_features) + ", " + str(len(self.time_slots)) + "timeslots")

    def init_training(self, DATA_ROOT):
        self.sequential_patient = None
        self.sequential_sec = 0

        featureFiles = {}
        patients = glob.glob(os.path.join(DATA_ROOT, self.features_directory + "/*/"))
        if not patients:
            print("No data found in " + DATA_ROOT)
        existing_features = {} # set
        for i, p in enumerate(patients):
            patient = os.path.basename(os.path.dirname(p))
            featureFiles[patient] = set()
            features = glob.glob(os.path.join(p, "*.bin"))
            for f in features:
                featureFiles[patient].add(os.path.basename(f))
            if i == 0:
                existing_features = featureFiles[patient]
            else:
                existing_features = existing_features.intersection(featureFiles[patient])

        self.data_dir = DATA_ROOT
        self.existing_features = sorted(list(existing_features)) #sorted list

        if self.train_sequences is None:
            self.patients = sorted(list(featureFiles.keys()))
        else:
            self.patients = self.train_sequences

        print("Found " + str(len(self.patients)) + " patients, and " + str(len(self.existing_features)) + " common features: " + str(self.existing_features) + ", " + str(len(self.time_slots)) + " timeslots")
        
        self.annotations = {}
        self.ann_arousal_indexes = {}
        self.ann_non_arousal_indexes = {}
        # load annotations in memory
        for pat in self.patients:
            ann_path = self.data_dir + '/' + self.features_directory + '/' + pat + '/' + pat + ".target"
            ann = np.fromfile(ann_path, dtype=np.int8).reshape(-1, 2)
            self.annotations[pat] = ann
            self.ann_arousal_indexes[pat] = np.where(ann[:, 0] == 1)[0]
            self.ann_non_arousal_indexes[pat] = np.where(ann[:, 0] == 0)[0]
        

    def get_random_batch2(self, batch):
        nninputs = np.zeros((batch, len(self.time_slots), len(self.existing_features)), dtype=np.float32)
        nntargets = np.zeros((batch, 2), dtype=np.int8)
        drop_invalid_arousals = True
        drop_invalid_sleepstages = True
        batch_id = -1

        arousal_ratio = 0.5
        while batch_id + 1 < batch: # Can't use 'for i in range()', because batch_id is modified
            batch_id += 1
            patient = random.sample(self.patients, 1)[0]
            
            ann = self.annotations[patient]
            arousal_indexes = self.ann_arousal_indexes[patient]
            non_arousal_indexes = self.ann_non_arousal_indexes[patient]
            
            selected_indexes = arousal_indexes if random.random() < arousal_ratio else non_arousal_indexes

            if len(selected_indexes) == 0:
                continue
            sec = np.random.choice(selected_indexes, 1)[0]
            if sec > len(ann) - 2:
                    continue
            nntarget = ann[sec]
            
            if drop_invalid_arousals and nntarget[0] == 2:
                batch_id -= 1
                continue
            if drop_invalid_sleepstages and nntarget[1] == 5:
                batch_id -= 1
                continue
            # print(patient + '/' + str(sec))
            nninput = np.zeros((len(self.time_slots), len(self.existing_features)), dtype=np.float32)
            for feature_i, feature_name in enumerate(self.existing_features):
                featureFilePath = self.data_dir + '/' + self.features_directory + '/' + patient + '/' + feature_name
                f = open(featureFilePath, 'rb')
                nninput[:,feature_i] = self._get_features_from_file(f, feature_name, sec, patient, 1)[0]
                f.close()
            # print("loaded data: ", nninput, " ", nntarget)
            nninputs[batch_id] = nninput
            nntargets[batch_id] = nntarget
        nntargets_arousal_onehot = convertToOneHot(nntargets[:, 0], 3) # Last is DontCare
        nntargets_sleep_stage_onehot = convertToOneHot(nntargets[:, 1], 6) # Last is Unknown
        return (nninputs, nntargets_arousal_onehot, nntargets_sleep_stage_onehot)

    # This is used during training.
    # Iteratively finds arousal samples to enrich 
    def get_random_batch(self, batch):
        nninputs = np.zeros((batch, len(self.time_slots), len(self.existing_features)), dtype=np.float32)
        nntargets = np.zeros((batch, 2), dtype=np.int8)
        drop_nonarousal_ratio = 0.25
        drop_invalid_arousals = True
        drop_invalid_sleepstages = False
        batch_id = -1
        next_item_has_to_be_arousal = False
        while batch_id + 1 < batch: # Can't use 'for i in range()', because batch_id is modified
            batch_id += 1
            patient = random.sample(self.patients, 1)[0]
            annotation_file = open(self.data_dir + '/' + self.features_directory + '/' + patient + '/' + patient + ".target", 'rb')
            num_seconds = os.fstat(annotation_file.fileno()).st_size // 2
            sec = random.randint(0, num_seconds - 2)
            annotation_file.seek(sec * 2)
            nntarget = np.frombuffer(annotation_file.read(2), dtype=np.int8)
            annotation_file.close()
            if drop_invalid_arousals and nntarget[0] == 2:
                batch_id -= 1
                continue
            if drop_invalid_sleepstages and nntarget[1] == 5:
                batch_id -= 1
                continue
            if next_item_has_to_be_arousal and nntarget[0] != 1:
                batch_id -= 1
                continue
            # print(patient + '/' + str(sec))
            nninput = np.zeros((len(self.time_slots), len(self.existing_features)), dtype=np.float32)
            for feature_i, feature_name in enumerate(self.existing_features):
                featureFilePath = self.data_dir + '/' + self.features_directory + '/' + patient + '/' + feature_name
                f = open(featureFilePath, 'rb')
                nninput[:,feature_i] = self._get_features_from_file(f, feature_name, sec, patient, 1)[0]
                f.close()
            # print("loaded data: ", nninput, " ", nntarget)
            nninputs[batch_id] = nninput
            nntargets[batch_id] = nntarget
            next_item_has_to_be_arousal = drop_nonarousal_ratio > 0 and random.random() < drop_nonarousal_ratio
        nntargets_arousal_onehot = convertToOneHot(nntargets[:, 0], 3) # Last is DontCare
        nntargets_sleep_stage_onehot = convertToOneHot(nntargets[:, 1], 6) # Last is Unknown
        return (nninputs, nntargets_arousal_onehot, nntargets_sleep_stage_onehot)
 
    def get_full_patients(self):
        choosen_patients = random.sample(self.patients, 40)
        nninputs = None
        targets = None
        for cp in choosen_patients:
            nninput, target = self.get_full_patient(cp)
            if nninputs is None:
                nninputs = nninput
                targets = target
            else:
                nninputs = np.concatenate((nninputs, nninput), axis=0)
                targets = np.concatenate((targets, target), axis=0)
        return (nninputs, targets)

    def get_full_patient(self, patient):
        drop_invalid_arousals = True
        print("loading ", patient)
        
        annotation_file = open(self.data_dir + '/' + self.features_directory + '/' + patient + '/' + patient + ".target", 'rb')
        nntarget = np.fromfile(annotation_file, dtype=np.int8).reshape((-1, 2))[:,0]
        annotation_file.close()

        nninputs = np.zeros((nntarget.shape[0], len(self.existing_features)), dtype=np.float32)

        for feature_i, feature_name in enumerate(self.existing_features):
            featureFilePath = self.data_dir + '/' + self.features_directory + '/' + patient + '/' + feature_name
            f = open(featureFilePath, 'rb')
            nninputs[:, feature_i] = np.fromfile(f, dtype=np.float32)
            f.close()
        #add time-diffs
        times = [-60, -30, -10, 0, 10, 30, 60]
        timed_out = None
        for time in times:
            if time > 0:
                diffed = np.zeros(nninputs.shape)
                diffed[time:, :] = nninputs[time:, :] - nninputs[:-time, :]
            elif time == 0:
                diffed = nninputs
            elif time < 0:
                diffed = np.zeros(nninputs.shape)
                diffed[:time, :] = nninputs[:time, :] - nninputs[-time:, :]
                
            if timed_out is None:
                timed_out = diffed
            else:
                timed_out = np.concatenate((timed_out, diffed), axis=1)

        nninputs = timed_out

        if drop_invalid_arousals:
            drop_mask = nntarget < 2
            nninputs = nninputs[drop_mask, :]
            nntarget = nntarget[drop_mask]

        return (nninputs, nntarget)

    def get_random_sequential_batch(self, batch):
        while True:
            pat = random.sample(self.patients, 1)[0]
            indexes = self.ann_arousal_indexes[pat] if random.random() > 0.5 else self.ann_non_arousal_indexes[pat]
            if len(indexes) > 0:
                break
        sec = np.random.choice(indexes, 1)[0]
        sec += random.randint(-batch + 1, 0)
        sec = np.clip(sec, 0, len(self.annotations[pat]) - batch)
        
        num_seconds = len(self.annotations[pat])
        if num_seconds == sec:
            return ([], [], [])
        
        if batch == -1:
            batch = num_seconds - sec
        else:
            batch = min(batch, num_seconds - self.sequential_sec)
        
        nntarget = self.annotations[pat][sec:sec + batch]
        
        myfeatures = []
        feature_path = self.data_dir + '/' + self.features_directory + '/' + pat
        for feature in self.existing_features:
            np_feature = np.fromfile(feature_path + '/' + feature, dtype=np.float32)
            sequence_len = len(np_feature)
            np_feature = np.pad(np_feature, 1, 'constant')
            myfeatures.append(np_feature)
        indexes = np.clip(self.time_slots + np.arange(sec + 1, sec + batch + 1)[None].T, 0, sequence_len + 1)
        nninput = np.array([f[indexes] for f in myfeatures])
        nninput = np.swapaxes(nninput.T, 0, 1)
        
        nntargets_arousal_onehot = convertToOneHot(nntarget[:, 0], 3)
        nntargets_sleep_stage_onehot = convertToOneHot(nntarget[:, 1], 6)
        return (nninput, nntargets_arousal_onehot, nntargets_sleep_stage_onehot)
        
    # get slice_count sequences in a batch
    def get_sliced_random_sequence_batch(self, batch = 1024, slice_count = 2):
        batch_sizes = [batch // slice_count] * (slice_count - 1) + [batch // slice_count + batch % slice_count]
    
        batch_array_0 = []
        batch_array_1 = []
        batch_array_2 = []
        batch_slices = 2
        
        # get n random sequences and concat them -> less correlation than one big sequence, but more time to load
        for small_batch_size in batch_sizes:
            batch_part = self.get_random_sequential_batch(small_batch_size)
            batch_array_0.append(batch_part[0])
            batch_array_1.append(batch_part[1])
            batch_array_2.append(batch_part[2])
        result = np.concatenate(batch_array_0, axis=0), np.concatenate(batch_array_1, axis=0), np.concatenate(batch_array_2, axis=0)
        return result


    # This is used when evaluating
    # batch = -1 means the whole sequence
    def get_sequential_batch(self, batch):
        if self.sequential_patient is None:
            self.sequential_patient = random.sample(self.patients, 1)[0]
            #self.sequential_patient = 'tr03-0103'
            self.sequential_sec = 0
        annotation_file = open(self.data_dir + '/' + self.features_directory + '/' + self.sequential_patient + '/' + self.sequential_patient + ".target", 'rb')
        num_seconds = os.fstat(annotation_file.fileno()).st_size // 2
        if num_seconds == self.sequential_sec:
            return ([], [], [])
        
        if batch == -1:
            batch = num_seconds - self.sequential_sec
        else:
            batch = min(batch, num_seconds - self.sequential_sec)

        annotation_file.seek(self.sequential_sec * 2)
        nntarget = np.frombuffer(annotation_file.read(batch * 2), dtype=np.int8).reshape((-1, 2))
        # num_seconds = os.fstat(annotation_file.fileno()).st_size // 2
        annotation_file.close()
        # print(self.sequential_patient + '/' + str(self.sequential_sec))
        nninput = np.zeros((batch, len(self.time_slots), len(self.existing_features)), dtype=np.float32)
        for i, v in enumerate(self.existing_features):
            f = open(self.data_dir + '/' + self.features_directory + '/' + self.sequential_patient + '/' + v, 'rb')
            nninput[:, :, i] = self._get_features_from_file(f, v, self.sequential_sec, self.sequential_patient, batch)
            f.close()
        self.sequential_sec += batch
        # print("loaded data: ", nninput, " ", nntarget)
        
        nntargets_arousal_onehot = convertToOneHot(nntarget[:, 0], 3)
        nntargets_sleep_stage_onehot = convertToOneHot(nntarget[:, 1], 6)
        return (nninput, nntargets_arousal_onehot, nntargets_sleep_stage_onehot)

    def reset_sequential_reader(self, patient=None):
        self.sequential_patient = patient
        self.sequential_sec = 0

    def _get_features_from_file(self, f, feature_name, second, patient, length=1):
        nninput = np.zeros((length, len(self.time_slots)), dtype=np.float32)
        num_seconds_in_feature_file = os.fstat(f.fileno()).st_size // 4
        for time_i, time_diff in enumerate(self.time_slots):
            skipped_head = (0 if second + time_diff >= 0 else -(second + time_diff))
            first_read_pos = max(0, second + time_diff)
            items_to_read = min(length - skipped_head, num_seconds_in_feature_file - first_read_pos)
            if items_to_read > 0:
                f.seek(first_read_pos * 4)
                try:
                    parsed = np.frombuffer(f.read(items_to_read * 4), dtype=np.float32)
                    #if np.any(np.isnan(parsed)):
                    #    print("There's NaN in ", patient, " ", feature_name, " second: ", second)
                    #    parsed = np.nan_to_num(parsed)
                    nninput[skipped_head : skipped_head+len(parsed), time_i] = parsed
                except:
                    print(patient + '/' + str(second))
                    raise
        return nninput


    # get targets for a whole sequence
    def get_sequence_targets(self, sequence_id):
        with open(self.data_dir + '/' + self.features_directory + '/' + sequence_id + '/' + sequence_id + ".target", 'rb') as annotation_file:
            num_seconds = os.fstat(annotation_file.fileno()).st_size // 2
            ann = np.frombuffer(annotation_file.read(num_seconds * 2), dtype=np.int8)
            ann = ann.reshape([-1, 2])
        return ann

    def get_sequence_features(self, feature_path):
        myfeatures = []
        for feature in self.existing_features:
            np_feature = np.fromfile(feature_path + '/' + feature, dtype=np.float32)
            sequence_len = len(np_feature)
            np_feature = np.pad(np_feature, 1, 'constant')
            myfeatures.append(np_feature)
        indexes = np.clip(self.time_slots + np.arange(1, sequence_len + 1)[None].T, 0, sequence_len + 1)
        inputs = np.array([f[indexes] for f in myfeatures])
        inputs = np.swapaxes(inputs.T, 0, 1)
        return inputs

    def patient_length_200hz_seq(self):
        return self.patient_length_200hz(self.data_dir + '/' + self.features_directory + '/' + self.sequential_patient)

    def patient_length_200hz(self, feature_path):
        #sig, fields = wfdb.rdsamp(self.data_dir + '/training/' + self.sequential_patient + "/" + self.sequential_patient) #This is slow a bit, some seconds
        #return sig.shape[0]
        f = open(feature_path + '/sample_count.txt', 'r')
        length = int(f.read())
        f.close()
        return length

def convertToOneHot(vector, num_classes=None):
    """
    Converts an input 1-D vector of integers into an output
    2-D array of one-hot vectors, where an i'th input value
    of j will set a '1' in the i'th row, j'th column of the
    output array.

    Example:
        v = np.array((1, 0, 4))
        one_hot_v = convertToOneHot(v)
        print one_hot_v

        [[0 1 0 0 0]
         [1 0 0 0 0]
         [0 0 0 0 1]]
    """

    assert isinstance(vector, np.ndarray)
    assert len(vector) > 0

    if num_classes is None:
        num_classes = np.max(vector)+1
    else:
        assert num_classes > 0
        assert num_classes >= np.max(vector)

    result = np.zeros(shape=(len(vector), num_classes))
    result[np.arange(len(vector)), vector] = 1
    return result.astype(int)
