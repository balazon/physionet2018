import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import precision_recall_curve, average_precision_score, auc, roc_auc_score
from dataloader import convertToOneHot
from itertools import cycle


def PR_curve(labels, probabilities, draw=True):
    if np.max(labels) == 0:
        # No arousals in annotation.
        print("Sequence contains only non-arousal regions - can't calculate AUPRC")
        return -1

    if np.any(np.isnan(probabilities)):
        print("NANs in probabilities")
        probabilities = np.nan_to_num(probabilities)

    precision, recall, thresholds = precision_recall_curve(labels, probabilities)

    auprc = auc(recall, precision)
    if np.isnan(auprc):
        print("auprc is nan")
        auprc = 0

    auroc = 0
    try:
        roc_auc_score(np.ravel(labels), probabilities)
    except:
        print("Error during roc_auc_score()")
    print(' AUPRC:%f' % (auprc))

    if draw:
        pr_auc = precision * recall
        op_index = np.argmax(pr_auc)
        operatingPointPrecision, operatingPointRecall, operatingPointThreshold = precision[op_index], recall[op_index], thresholds[op_index]
        op_auc = pr_auc[op_index]
        # print("PR auc for operating point: {}".format(op_auc))

        average_precision = average_precision_score(labels, probabilities)
        plt.step(recall, precision, color='b', alpha=0.2, where='post')
        plt.fill_between(recall, precision, step='post', alpha=0.2, color='b')

        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.ylim([0.0, 1.05])
        plt.xlim([0.0, 1.0])
        plt.title('Arousal Precision-Recall curve AP={0:0.2f}'.format(average_precision))

        plt.text(operatingPointRecall, operatingPointPrecision, "AUC={:0.2f}".format(op_auc))
        
        plt.plot([operatingPointRecall], [operatingPointPrecision], 'bo')
        
        plt.show()

    return auprc

# probabilities: (None, K) shape with each row specifying the probability for each class
def PR_curve_multiclass(labels, probabilities):
    n_classes = probabilities.shape[1]
    labels = convertToOneHot(labels, n_classes)
    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    for i in range(n_classes):
        precision[i], recall[i], _ = precision_recall_curve(labels[:, i],
                                                            probabilities[:, i])
        average_precision[i] = average_precision_score(labels[:, i], probabilities[:, i])

    # A "micro-average": quantifying score on all classes jointly
    precision["micro"], recall["micro"], _ = precision_recall_curve(labels.ravel(),
        probabilities.ravel())
    average_precision["micro"] = average_precision_score(labels, probabilities,
                                                        average="micro")
    # print('Average precision score, micro-averaged over all classes: {0:0.2f}'
    #     .format(average_precision["micro"]))

    
    # setup plot details
    colors = cycle(['navy', 'turquoise', 'darkorange', 'cornflowerblue', 'teal', 'magenta', 'brown'])

    plt.figure(figsize=(7, 8))
    f_scores = np.linspace(0.2, 0.8, num=4)
    lines = []
    labels = []
    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

    lines.append(l)
    labels.append('iso-f1 curves')
    l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
    lines.append(l)
    labels.append('micro-average Precision-recall (area = {0:0.2f})'
                ''.format(average_precision["micro"]))

    for i, color in zip(range(n_classes), colors):
        l, = plt.plot(recall[i], precision[i], color=color, lw=2)
        lines.append(l)
        labels.append('Precision-recall for class {0} (area = {1:0.2f})'
                    ''.format(i, average_precision[i]))

    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.25)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Sleep stage Precision-Recall')
    plt.legend(lines, labels, loc=(0, -.38), prop=dict(size=14))


    plt.show()

if __name__ == "__main__":
    labels = np.array([0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
    probabilities = np.array([0.1, 0.4, 0.35, 0.8, 0.1, 0.3, 0.4, 0.3, 0.6, 0.55, 0.3, 0.1, 0.12, 0.22, 0.7, 0.77, 0.9, 0.25, 0.82, 0.98, 0.91, 0.92, 0.82, 0.65, 0.68, 0.94, 0.96, 0.97, 0.978, 0.99])
    # probabilities *= 0.5
    PR_curve(labels, probabilities)