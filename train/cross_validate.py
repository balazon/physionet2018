import os
import train
from random import shuffle
import numpy as np

DATA_DIR="/home/AD.ADASWORKS.COM/marton.gorog/Desktop/"
nof_sections=10

dirs = [name for name in os.listdir(DATA_DIR + '/training_features/') if os.path.isdir(DATA_DIR + '/training_features/' + name)]
shuffle(dirs)
chunks = {}
dirs_per_chunk = len(dirs) // nof_sections
for i in range(nof_sections):
    chunks[i] = dirs[i*dirs_per_chunk : (i+1)*dirs_per_chunk]
auprcs = []

for i in range(nof_sections):
    train_seq = []
    for j in range(nof_sections):
        if i != j:
            train_seq.extend(chunks[j])
    test_seq = chunks[i]
    trainer = train.Trainer(DATA_DIR, "crossvalidate_{0}".format(i), model=None, sleepstage_weight=0.5, train_sequences=train_seq, test_sequences=test_seq)
    trainer.run()
    auprcs.append(trainer.mean_auprc)

print(auprcs)
print("MEAN AUPRC: ", np.mean(np.array(auprcs)))
