import matplotlib.pyplot as plt
import numpy as np

data_dir = "/opt/work/physionet2018/models/dusitas03true/"
file_path = data_dir + '/tr03-1143_whole_file_eval.bin'

file = open(file_path, 'r')
data = np.fromfile(file, dtype=np.int8).astype(np.float32)
file.close()
#in file:
#0 GT/target arousal 0-2
#1 GT/target sleepstage 0-5
#2 pred arousal index 0-1
#3 pred sleepstage 0-4
#4 pred arousal probability 0-100 %

data = data.reshape((-1, 5))
data[data==2] = 0.5 # for better visualization, invalid arousal is displayed as 0.5

plt.figure(1)
titles = ['GT/target arousal', 'GT/target sleepstage', 'pred arousal', 'pred sleepstage']
ax = None
for i in range(4):
    ax = plt.subplot(4, 1, i+1, sharex=ax)
    plt.title(titles[i])
    ax.plot(data[:,i])
    if i%2 == 0:
        ax.plot(data[:,4]/100.0)
plt.show()

"""
plt.figure(2)

ax = plt.subplot(2, 1, 1)
plt.title("Arousal")
correct = np.where((data[:,0] == 2))[0]
ax.plot(correct, data[:,2][correct], 'b.', ms=1, label='not annotated')
correct = np.where((data[:,0] < 2) * (data[:,0] - data[:,2] != 0))[0]
ax.plot(correct, data[:,2][correct], 'r.', ms=1, label='wrong prediction')
#ax.plot(correct, data[:,0][correct], 'y.', ms=1, label='wrong prediction')
correct = np.where((data[:,0] - data[:,2] == 0))[0]
plt.legend()

ax = plt.subplot(2, 1, 2, sharex=ax)
plt.title("Sleep stage")
correct = np.where((data[:,1] == 6))[0]
ax.plot(correct, data[:,3][correct], 'b.', ms=1, label='not annotated')
correct = np.where((data[:,1] < 6) * (data[:,1] - data[:,3] != 0))[0]
ax.plot(correct, data[:,3][correct], 'r.', ms=1, label='wrong prediction')
#ax.plot(correct, data[:,0][correct], 'y.', ms=1, label='wrong prediction')
correct = np.where((data[:,1] - data[:,3] == 0))[0]

plt.legend()
plt.show()
"""
