"""A deep MNIST classifier using convolutional layers.
See extensive documentation at https://www.tensorflow.org/get_started/mnist/pros
"""
# Disable linter warnings to maintain consistency with tutorial.
# pylint: disable=invalid-name

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import sys
import tempfile
import tensorflow as tf
import numpy as np
import glob

sys.path.append('./train')
sys.path.append('..')
# from .dataloader import dataloader
from dataloader import dataloader

import time
import os
import tqdm
from tensorflow.contrib import slim

try:
    from stats import PR_curve, PR_curve_multiclass
except:
    print("Couldn't load stats (probably sklearn is missing), but it's okay if you are only inferring")
import score2018


BATCH_SIZE = 20
MAX_ITER = 300000
SAVE_FREQUENCY = 1000 # after every {} iterations, save a checkpoint
GPU_COUNT = 0

class Trainer:
    def __init__(self, data_dir = './data', run_name = "test", model = None, sleepstage_weight = 0.5, draw_prc = 0, train_sequences=None,
     test_sequences=None, draw_first_layer = 0, probabilities_to_extreme=1.0, post_scale_weight=0.0, post_offset_weight=0.0):
        self.data_dir = data_dir
        self.run_name = run_name
        self.model = model
        self.sleepstage_weight = sleepstage_weight
        self.train_sequences = train_sequences
        self.test_sequences = test_sequences
        self.draw_prc = draw_prc
        self.data_loader = dataloader(self.train_sequences)
        self.draw_first_layer = draw_first_layer
        self.probabilities_to_extreme = probabilities_to_extreme
        self.post_scale_weight = post_scale_weight
        self.post_offset_weight = post_offset_weight
        self.tree = None
        self.official_scorer = score2018.Challenge2018Score()

    def deepnn(self, x):
        in_timeslots = x.shape[1].value
        in_features = x.shape[2].value
        # Reshape to use within a convolutional neural net.
        with tf.name_scope('reshape'):
            x_image = tf.reshape(x, [-1, in_timeslots, in_features, 1])

        with tf.name_scope('conv1'):
            #first_layer = slim.separable_convolution2d(x_image, num_outputs=None, kernel_size=[1, in_timeslots], depth_multiplier=8, stride=[in_timeslots, in_timeslots])
            first_layer = slim.conv2d(x_image, num_outputs=32, kernel_size=[in_timeslots, 1], stride=[in_timeslots, 1])
            conv2_flat = tf.reshape(first_layer, [-1, 32 * in_features])


        out_classes = 2 + 5
        with tf.name_scope('fc2'):
            fc = slim.fully_connected(conv2_flat, 128)
            y_conv = slim.fully_connected(fc, out_classes, activation_fn=None)
        y_arousal, y_sleepstages = tf.split(y_conv, [2, 5], axis=1)

        return y_arousal, y_sleepstages

    def init_vars(self, saver, sess):
        model = None
        glob_step = 0

        self.checkpoint_dir = "models/{}/checkpoints".format(self.run_name)
        if self.model is None:
            model = tf.train.latest_checkpoint(self.checkpoint_dir)
        else:
            model = "{}/{}".format(self.checkpoint_dir, self.model)
            if not os.path.isfile(model + ".meta") or not os.path.isfile(model + ".index") or not os.path.isfile(model + ".data-00000-of-00001"):
                raise Exception("can't find the specified model - {}".format(model))
        if model is not None:
            glob_step = int(model[model.rfind('-') + 1:])
            print("restoring model: {}".format(model))
            saver.restore(sess, model)
        else:
            print("initializing new model")
            sess.run(tf.global_variables_initializer())
        return glob_step

    def init_infer(self):
        
        tf.reset_default_graph()
        self.x = tf.placeholder(tf.float32, [None, len(self.data_loader.time_slots), len(self.data_loader.existing_features)], name='nninput')
        y_pred_arousal, y_pred_sleepstages = self.deepnn(self.x)
        self.softmaxed_arousal = tf.nn.softmax(y_pred_arousal)
        self.softmaxed_sleepstages = tf.nn.softmax(y_pred_sleepstages)
        
        self.saver = tf.train.Saver()

    def infer(self, sequence_path):
        self.data_loader.reset_sequential_reader(sequence_path)
        arousal_probabilities = np.zeros((0), dtype=np.float32)

        nninput = self.data_loader.get_sequence_features(sequence_path)

        with tf.Session() as self.sess:
            _ = self.init_vars(self.saver, self.sess)
            softmaxed_arousal_out, _ = self.sess.run(
                [self.softmaxed_arousal, self.softmaxed_sleepstages], 
                feed_dict={self.x: nninput})
                
        arousal_probabilities = np.concatenate((arousal_probabilities, softmaxed_arousal_out[:,1]), axis=0)
        if self.probabilities_to_extreme != 1.0:
            arousal_probabilities = np.maximum(0, np.minimum(1, ((arousal_probabilities - 0.5) * self.probabilities_to_extreme + 0.5)))
        final_result = self.rescale_and_extend(arousal_probabilities, self.data_loader.patient_length_200hz(sequence_path))
        return final_result

    def run_tree(self):
        from sklearn.ensemble import GradientBoostingClassifier

        self.data_loader.init_training(self.data_dir)

        # Create the model
        #self.x = tf.placeholder(tf.float32, [None, len(self.data_loader.time_slots), len(self.data_loader.existing_features)], name='nninput')

        # Define loss and optimizer
        #self.y_target_arousal = tf.placeholder(tf.float32, [None, 3], 'arousal_gt') # The last is the unknown flag
        #self.y_target_sleepstage = tf.placeholder(tf.float32, [None, 6], name='sleepstage_gt') # The last is the unknown flag

        self.model_dir = "models/{}/".format(self.run_name)
        if not os.path.exists(self.model_dir):
            os.makedirs(self.model_dir)
        logdir = self.model_dir + "/logs"
        print('Saving graph to: %s' % logdir)

        print("--== TRAINING ==--")
        (nninputs, nntargets) = self.data_loader.get_full_patients()
        self.tree = GradientBoostingClassifier(n_estimators=20, learning_rate=0.5, subsample=1, max_depth=3, random_state=0)
        self.tree.fit(nninputs, nntargets)
        
        print("--== EVALUATING ON FULL PATIENTS ==--")
        self.mean_accuracies = np.zeros((0,2), dtype=np.float32)

        arousal_probabilities = np.zeros((0), dtype=np.float32)
        self.auprcs = []

        for pat in self.test_sequences:
            prob_ar, gt_ar = self.evaluate_on_full_patient_tree(pat)
            
            if self.probabilities_to_extreme != 1.0:
                prob_ar = np.maximum(0, np.minimum(1, ((prob_ar - 0.5) * self.probabilities_to_extreme + 0.5)))
            self.draw_PR_curves(pat, prob_ar, None, gt_ar, None)
            
            arousal_probabilities = np.concatenate((arousal_probabilities, prob_ar), axis=0)        

        f = open(self.model_dir + "/average_prc_test.txt", 'w')
        print("{0:.5f}".format(np.mean(np.array(self.auprcs))), file=f)
        f.close()

        f = open(self.model_dir + "/average_accuracy_test.txt", 'w')
        self.mean_auprc = np.mean(self.mean_accuracies[:,0])
        print("%.*g\t%.*g" % (3, self.mean_auprc, 3, np.mean(self.mean_accuracies[:,1])), file=f)
        f.close()

        
        # run the same thing on training sequences
        evaluate_on_whole_training_set = False
        if evaluate_on_whole_training_set:
            arousal_probabilities = np.zeros((0), dtype=np.float32)
            self.auprcs = []
            for pat in self.train_sequences:
                prob_ar, gt_ar = self.evaluate_on_full_patient_tree(pat)
                self.draw_PR_curves(pat, prob_ar, None, gt_ar, None)

                arousal_probabilities = np.concatenate((arousal_probabilities, prob_ar), axis=0)

            f = open(self.model_dir + "/average_prc_train.txt", 'w')
            print("{0:.5f}".format(np.mean(np.array(self.auprcs))), file=f)
            f.close()

            f = open(self.model_dir + "/average_accuracy_train.txt", 'w')
            self.mean_auprc = np.mean(self.mean_accuracies[:,0])
            print("%.*g\t%.*g" % (3, self.mean_auprc, 3, np.mean(self.mean_accuracies[:,1])), file=f)
            f.close()

        
    def run(self):
        self.data_loader.init_training(self.data_dir)

        tf.reset_default_graph()

        # Create the model
        self.x = tf.placeholder(tf.float32, [None, len(self.data_loader.time_slots), len(self.data_loader.existing_features)], name='nninput')

        # Define loss and optimizer
        self.y_target_arousal = tf.placeholder(tf.float32, [None, 3], 'arousal_gt') # The last is the unknown flag
        self.y_target_sleepstage = tf.placeholder(tf.float32, [None, 6], name='sleepstage_gt') # The last is the unknown flag

        # Build the graph for the deep net
        self.y_pred_arousal, self.y_pred_sleepstages = self.deepnn(self.x)

        with tf.name_scope('loss'):
            arousal_not_invalid_mask = tf.equal(self.y_target_arousal[:, 2], 0)
            sleepstage_not_invalid_mask = tf.equal(self.y_target_sleepstage[:, 5], 0)

            # This simplest version doesn't handle invalid (ignore) items, so training will explode
            #TODO: With TF older than 1.7, cut the '_v2' from the two lines below. Actually TF 1.7 can work without '_v2' as well, only gives a warning.
            #self.cross_entropy_arousal = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y_target_arousal[:, 0:2], logits=self.y_pred_arousal)
            #self.cross_entropy_sleepstage = tf.nn.softmax_cross_entropy_with_logits_v2(labels=self.y_target_sleepstage[:, 0:5], logits=self.y_pred_sleepstages)

            self.cross_entropy_arousal = tf.losses.sparse_softmax_cross_entropy(labels=tf.argmax(self.y_target_arousal[:, 0:2], axis=1), logits=self.y_pred_arousal, weights=tf.cast(arousal_not_invalid_mask, tf.float32))
            self.cross_entropy_sleepstage = tf.losses.sparse_softmax_cross_entropy(labels=tf.argmax(self.y_target_sleepstage[:, 0:5], axis=1), logits=self.y_pred_sleepstages, weights=tf.cast(sleepstage_not_invalid_mask, tf.float32))
            
            # Zero out those after cross-entropy. Doesn't work somehow - AttributeError: 'Tensor' object has no attribute '_lazy_read'
            #invalid_indices = tf.where(tf.equal(self.y_target_sleepstage[:,5], 1))[:,0]
            #self.cross_entropy_sleepstage = tf.scatter_update(self.cross_entropy_sleepstage, invalid_indices, tf.constant(0))
            
            #cross_entropy_arousal = tf.Print(cross_entropy_arousal, [cross_entropy_arousal, cross_entropy_sleepstage, y_target_arousal[0, 0:3], y_pred_arousal[0,:], y_target_sleepstage[0, 0:6], y_pred_sleepstages[0,:]], summarize=20)
            self.cross_entropy = tf.reduce_mean(self.cross_entropy_arousal + self.sleepstage_weight * self.cross_entropy_sleepstage)

        global_step = tf.Variable(0, name='global_step', trainable=False)
        with tf.name_scope('optimizer'):
            starter_learning_rate = 1e-3
            learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step, 5000, 0.9, staircase=True)

            self.train_step = tf.train.AdamOptimizer(learning_rate).minimize(self.cross_entropy, global_step=global_step)
            #self.train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(self.cross_entropy, global_step=global_step) #e-4 is too slow to start
            #self.train_step = tf.train.MomentumOptimizer(learning_rate, momentum=0.9).minimize(self.cross_entropy, global_step=global_step)

        with tf.name_scope('accuracy'):
            self.correct_prediction_arousal = tf.boolean_mask(tf.equal(tf.argmax(self.y_pred_arousal, 1), tf.argmax(self.y_target_arousal[:, 0:2], 1)), arousal_not_invalid_mask)
            self.correct_prediction_sleepstage = tf.boolean_mask(tf.equal(tf.argmax(self.y_pred_sleepstages, 1), tf.argmax(self.y_target_sleepstage[:, 0:5], 1)), sleepstage_not_invalid_mask)
        
            self.correct_prediction_arousal_float = tf.cast(self.correct_prediction_arousal, tf.float32)
            self.correct_prediction_sleepstage_float = tf.cast(self.correct_prediction_sleepstage, tf.float32)
            self.check = tf.add_check_numerics_ops()
            self.accuracy_a = tf.cond(tf.shape(self.correct_prediction_arousal_float)[0] > 0, lambda: tf.reduce_mean(self.correct_prediction_arousal_float), lambda: -1.0)
            self.accuracy_s = tf.cond(tf.shape(self.correct_prediction_sleepstage_float)[0] > 0, lambda: tf.reduce_mean(self.correct_prediction_sleepstage_float), lambda: -1.0)
    
        self.softmaxed_arousal = tf.nn.softmax(self.y_pred_arousal)
        self.softmaxed_sleepstages = tf.nn.softmax(self.y_pred_sleepstages)

        tf.summary.scalar("loss", self.cross_entropy)
        merged_summary_op = tf.summary.merge_all()

        self.model_dir = "models/{}/".format(self.run_name)
        logdir = self.model_dir + "/logs"
        print('Saving graph to: %s' % logdir)
        train_writer = tf.summary.FileWriter(logdir)
        train_writer.add_graph(tf.get_default_graph())
    
        # Forcing CPU
        config = tf.ConfigProto(
            device_count = {'GPU': GPU_COUNT}
        )
        config.gpu_options.allow_growth = True

        saver = tf.train.Saver(max_to_keep=50)
    
        with tf.Session(config=config) as self.sess:
            glob_step = self.init_vars(saver, self.sess)

            if self.draw_first_layer:
                self.visualize_first_layer(self.sess)

            print("--== TRAINING ==--")
            for i in tqdm.tqdm(range(glob_step, MAX_ITER)):
                #start = time.time()
                if MAX_ITER - i < 10000 and False:
                    if self.sleepstage_weight > 0.0:
                        print("Settings sleepstage_weight to 0")
                    self.sleepstage_weight = 0.0
                
                batch = self.data_loader.get_random_batch(BATCH_SIZE)
                # batch = self.data_loader.get_random_batch2(BATCH_SIZE)
                # batch = self.data_loader.get_random_sequential_batch(BATCH_SIZE)
                # batch = self.data_loader.get_sliced_random_sequence_batch(BATCH_SIZE, slice_count = 2)
                
                #print(time.time() - start)

                #start = time.time()
                _, loss, summary = self.sess.run([self.train_step, self.cross_entropy, merged_summary_op], 
                                                    feed_dict={self.x: batch[0], self.y_target_arousal: batch[1], self.y_target_sleepstage: batch[2]})
                #print("         ", time.time() - start)
                train_writer.add_summary(summary, i + 1)

                if (i + 1) % 100 == 0:
                    train_accuracy_a, train_accuracy_s = self.sess.run([self.accuracy_a, self.accuracy_s], feed_dict={self.x: batch[0], self.y_target_arousal: batch[1], self.y_target_sleepstage: batch[2]})
                    print('step {0}, training accuracy {1:.2f} {2:.2f}\tLOSS: {3:.3f}'.format(i + 1, train_accuracy_a, train_accuracy_s, loss))
            
                if (i + 1) % SAVE_FREQUENCY == 0 or i == MAX_ITER-1:
                    save_path = saver.save(self.sess, "models/{}/checkpoints/model".format(self.run_name), global_step=(i + 1))
                    print("Model saved in path: {}".format(save_path))

            print("--== EVALUATING ON FULL PATIENTS ==--")
            self.mean_accuracies = np.zeros((0,2), dtype=np.float32)

            arousal_probabilities = np.zeros((0), dtype=np.float32)
            sleep_stage_probabilities = np.zeros((0, 5), dtype=np.float32)
            self.auprcs = []

            for pat in self.test_sequences:
                prob_ar, prob_sl, gt_ar, gt_sl = self.evaluate_on_full_patient(pat)
                if self.probabilities_to_extreme != 1.0:
                    prob_ar = np.maximum(0, np.minimum(1, ((prob_ar - 0.5) * self.probabilities_to_extreme + 0.5)))
                self.draw_PR_curves(pat, prob_ar, prob_sl, gt_ar, gt_sl)
                
                arousal_probabilities = np.concatenate((arousal_probabilities, prob_ar), axis=0)
                sleep_stage_probabilities = np.concatenate((sleep_stage_probabilities, prob_sl))

            # self.draw_PR_curves(eval_patients, arousal_probabilities, sleep_stage_probabilities)

            print("official ", self.official_scorer.gross_auprc())
            print("old 1by1 ", np.mean(np.array(self.auprcs)))

            f = open(self.model_dir + "/average_prc_test.txt", 'w')
            print("official {0:.5f}".format(self.official_scorer.gross_auprc()), file=f)
            print("old 1by1 {0:.5f}\n".format(np.mean(np.array(self.auprcs))), file=f)
            f.close()

            f = open(self.model_dir + "/average_accuracy_test.txt", 'w')
            self.mean_auprc = np.mean(self.mean_accuracies[:,0])
            print("%.*g\t%.*g" % (3, self.mean_auprc, 3, np.mean(self.mean_accuracies[:,1])), file=f)
            f.close()

            
            # run the same thing on training sequences
            evaluate_on_whole_training_set = False
            if evaluate_on_whole_training_set:
                arousal_probabilities = np.zeros((0), dtype=np.float32)
                sleep_stage_probabilities = np.zeros((0, 5), dtype=np.float32)
                self.auprcs = []
                for pat in self.train_sequences:
                    prob_ar, prob_sl, gt_ar, gt_sl = self.evaluate_on_full_patient(pat)
                    self.draw_PR_curves(pat, prob_ar, prob_sl, gt_ar, gt_sl)

                    arousal_probabilities = np.concatenate((arousal_probabilities, prob_ar), axis=0)
                    sleep_stage_probabilities = np.concatenate((sleep_stage_probabilities, prob_sl))

                f = open(self.model_dir + "/average_prc_train.txt", 'w')
                print("{0:.5f}".format(np.mean(np.array(self.auprcs))), file=f)
                f.close()

                f = open(self.model_dir + "/average_accuracy_train.txt", 'w')
                self.mean_auprc = np.mean(self.mean_accuracies[:,0])
                print("%.*g\t%.*g" % (3, self.mean_auprc, 3, np.mean(self.mean_accuracies[:,1])), file=f)
                f.close()


    def evaluate_on_full_patient(self, patient):
        self.data_loader.reset_sequential_reader(patient)
        binary_result_array = None #This is a 4 column array, contains: Arousal GT, SleepStage GT, Arousal Pred, SleepStage Pred. Saved for visualization with 'draw_whole_eval.py'
        accuracies_a = [] # Gathered during whole sequence
        accuracies_s = [] # Gathered during whole sequence
        arousal_probabilities = np.zeros((0), dtype=np.float32)
        sleep_stage_probabilities = np.zeros((0, 5), dtype=np.float32)

        while True:
            batch = self.data_loader.get_sequential_batch(-1)
            if len(batch) == 0 or len(batch[0]) == 0:
                break
            accuracy_a_out, accuracy_s_out, y_pred_arousal_out, y_pred_sleepstages_out, softmaxed_arousal_out, softmaxed_sleepstage_out = self.sess.run(
                [self.accuracy_a, self.accuracy_s, self.y_pred_arousal, self.y_pred_sleepstages, self.softmaxed_arousal, self.softmaxed_sleepstages], 
                feed_dict={self.x: batch[0], self.y_target_arousal: batch[1], self.y_target_sleepstage: batch[2]})
            if accuracy_a_out >= 0:
                accuracies_a.append(accuracy_a_out)
            if accuracy_s_out >= 0:
                accuracies_s.append(accuracy_s_out)
            binary_result = np.stack((np.argmax(batch[1], axis=1), np.argmax(batch[2], axis=1), np.argmax(y_pred_arousal_out, axis=1), np.argmax(y_pred_sleepstages_out, axis=1), softmaxed_arousal_out[:,1]*100.0), axis=1)
            arousal_probabilities = np.concatenate((arousal_probabilities, softmaxed_arousal_out[:,1]), axis=0)
            sleep_stage_probabilities = np.concatenate((sleep_stage_probabilities, softmaxed_sleepstage_out))
            
            if binary_result_array is None:
                binary_result_array = binary_result
            else:
                binary_result_array = np.concatenate((binary_result_array, binary_result), axis=0)
            
        #TODO: "test/te09-0094.mat should have a corresponding file named annotations/te09-0094.vec"
        np.savetxt(self.model_dir + '/' + self.data_loader.sequential_patient + '_1hz.vec', arousal_probabilities, fmt='%.3f')

        #final_result = self.rescale_and_extend(arousal_probabilities, self.data_loader.patient_length_200hz_seq())
        #np.savetxt(self.model_dir + '/' + self.data_loader.sequential_patient + '_200hz.vec', final_result, fmt='%.3f') # This is really slow, can be ~30 seconds
        
        mean_accuracy_a = np.mean(np.array(accuracies_a))
        mean_accuracy_s = np.mean(np.array(accuracies_s))
        self.mean_accuracies = np.concatenate((self.mean_accuracies, np.expand_dims(np.array([mean_accuracy_a, mean_accuracy_s]), axis=0)))

        f = open(self.model_dir + "/" + self.data_loader.sequential_patient + '_whole_sequence_accuracy.txt', 'w')
        print("%.*g\t%.*g" % (3, mean_accuracy_a, 3, mean_accuracy_s), file=f)
        f.close()

        binary_result_array = binary_result_array.astype(np.int8)
        f = open(self.model_dir + "/" + self.data_loader.sequential_patient + '_whole_file_eval.bin', 'w')
        binary_result_array.tofile(f)
        f.close()
        return arousal_probabilities, sleep_stage_probabilities, binary_result_array[:,0], binary_result_array[:,1]

    def evaluate_on_full_patient_tree(self, patient):
        self.data_loader.reset_sequential_reader(patient)
        binary_result_array = None #This is a 4 column array, contains: Arousal GT, SleepStage GT, Arousal Pred, SleepStage Pred. Saved for visualization with 'draw_whole_eval.py'
        arousal_probabilities = np.zeros((0), dtype=np.float32)
        
        (nninputs, nntargets) = self.data_loader.get_full_patient(patient)
        y_pred_arousal_out = self.tree.predict_proba(nninputs)[:,0]
        binary_result = y_pred_arousal_out
        arousal_probabilities = np.concatenate((arousal_probabilities, y_pred_arousal_out.ravel()), axis=0)
        
        if binary_result_array is None:
            binary_result_array = binary_result
        else:
            binary_result_array = np.concatenate((binary_result_array, binary_result), axis=0)
            
        #TODO: "test/te09-0094.mat should have a corresponding file named annotations/te09-0094.vec"
        np.savetxt(self.model_dir + '/' + self.data_loader.sequential_patient + '_1hz.vec', arousal_probabilities, fmt='%.3f')

        #final_result = self.rescale_and_extend(arousal_probabilities, self.data_loader.patient_length_200hz_seq())
        #np.savetxt(self.model_dir + '/' + self.data_loader.sequential_patient + '_200hz.vec', final_result, fmt='%.3f') # This is really slow, can be ~30 seconds
        
        binary_result_array = binary_result_array.astype(np.int8)
        f = open(self.model_dir + "/" + self.data_loader.sequential_patient + '_whole_file_eval.bin', 'w')
        binary_result_array.tofile(f)
        f.close()
        return arousal_probabilities, nntargets

    def running_mean(self, x, N):
        cumsum = np.cumsum(np.insert(x, 0, 0)) 
        avg = (cumsum[N:] - cumsum[:-N]) / float(N)
        pre = np.repeat(avg[0], int(N/2))
        return np.concatenate((pre, avg, np.repeat(avg[-1], len(x) - len(avg) - len(pre) )))

    def draw_PR_curves(self, patients, arousal_probabilities, sleep_stage_probabilities, gt_ar, gt_sl):
        if not isinstance(patients, list): patients = [patients]
            
        valid_sample_indexes = np.where(gt_ar != 2)[0] # no ignore labels
        
        sequence_targets_valid = gt_ar[valid_sample_indexes]
        arousal_probs_valid = arousal_probabilities[valid_sample_indexes]
        
        auprc = PR_curve(sequence_targets_valid, arousal_probs_valid, self.draw_prc == 1)
        if np.isnan(auprc) or auprc == -1:
            return
        
        #Postprocess
        if self.post_offset_weight != 0.0 or self.post_scale_weight != 0.0:
            window_in_seconds = 15
            avg_series = self.running_mean(arousal_probabilities, window_in_seconds)
            multiplier = 1 - self.post_scale_weight  + self.post_scale_weight * (abs(avg_series - 0.5) + 1)
            offset = (avg_series - 0.5) * self.post_offset_weight
            arousal_probabilities = np.clip(multiplier * (offset*multiplier/10 + arousal_probabilities - 0.5) + 0.5, 0, 1)

        self.official_scorer.score_record(gt_ar, arousal_probabilities, patients[0])
        auprc = self.official_scorer.record_auprc(patients[0])
        print('official %-11s  %8.6f' % (patients[0], auprc))

        self.auprcs.append(auprc)

        if self.draw_prc == 1:
            valid_sleepsample_indexes = np.where(gt_sl != 5)[0]
            sleep_stage_probs_valid = sleep_stage_probabilities[valid_sleepsample_indexes]
            sequence_targets_sleep_valid = gt_sl[valid_sleepsample_indexes]
            PR_curve_multiclass(sequence_targets_sleep_valid, sleep_stage_probs_valid)

    def rescale_and_extend(self, data1hz, target_length):
        res = np.zeros((target_length), dtype=np.float32)
        assert len(data1hz)*200 <= target_length
        for i, v in enumerate(data1hz):
            res[200*i : 200*(i+1)] = v
        res[len(data1hz)*200 : target_length] = data1hz[-1]
        return res

    def visualize_first_layer(self, sess):
        myvars = tf.get_collection(key = tf.GraphKeys.GLOBAL_VARIABLES)
        
        first = np.reshape(sess.run(myvars[0]), [21, 32])
        import cv2
        first -= np.min(first)
        first *= 255.0 / np.max(first)

        img = first.astype(np.uint8)
        img = cv2.resize(img, (32 * 16, 21 * 16), interpolation=cv2.INTER_NEAREST)
        img = np.transpose(img)
        cv2.imshow("asd", img)
        cv2.waitKey(0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    #parser.add_argument('--data_dir', type=str, default='d:\\Marci\\Prog\\Physionet2018\\', help='Directory for storing input data')
    parser.add_argument('--data_dir', type=str, default='./data/', help='Directory for storing input data')
    parser.add_argument('--run_name', type=str, default='test_2', help='')
    parser.add_argument('--model', type=str, default=None, help='model checkpoint to restore, eg.: model-10000')
    parser.add_argument('--sleepstage_weight', type=float, default=0.4, help='Between 0-1')
    parser.add_argument('--probabilities_to_extreme', type=float, default=1.0, help='1.0 has no effect')
    parser.add_argument('--post_scale_weight', type=float, default=0.0, help='0.0 has no effect')
    parser.add_argument('--post_offset_weight', type=float, default=0.0, help='0.0 has no effect')
    parser.add_argument('--draw_prc', type=int, default=0, help='0 non-interactive,  1 draw & wait to close window')
    parser.add_argument('--draw_first_layer', type=int, default=0, help='0: off, 1: draw first layer of weights, wait to close window')
    parser.add_argument('--train_on_all', type=int, default=0, help='0 dont train on test data,  1 train also on test data')
    FLAGS, unparsed = parser.parse_known_args()
    # test_seq = ["tr03-0079", "tr03-0560", "tr03-1143", "tr03-1389", "tr05-0348", "tr06-0584", "tr11-0200", "tr14-0240"]
    test_seq = ["tr03-0304", "tr03-0413", "tr03-0726", "tr03-1119", "tr03-1231", "tr03-1294", "tr04-0015", "tr04-0020", "tr04-0209", "tr04-0265", "tr04-0362", "tr04-0402", "tr04-0658", "tr04-0703", "tr04-0963", "tr05-0260", "tr05-0315", "tr05-0418", "tr05-0629", "tr05-0647", "tr05-0707", "tr05-0724", "tr05-0738", "tr05-0744", "tr05-0812", "tr05-0860", "tr05-0932", "tr05-1277", "tr05-1313", "tr05-1371", "tr05-1547", "tr05-1575", "tr05-1689", "tr06-0122", "tr06-0193", "tr06-0249", "tr06-0347", "tr06-0397", "tr06-0429", "tr06-0694", "tr06-0773", "tr06-0862", "tr06-0880", "tr06-1123", "tr07-0016", "tr07-0036", "tr07-0102", "tr07-0105", "tr07-0197", "tr07-0247", "tr07-0549", "tr07-0579", "tr07-0594", "tr07-0687", "tr07-0865", "tr07-0891", "tr07-0895", "tr07-0909", "tr09-0436", "tr10-0006", "tr10-0592", "tr11-0042", "tr11-0200", "tr11-0471", "tr11-0516", "tr11-0584", "tr11-0644", "tr12-0135", "tr12-0209", "tr12-0229", "tr12-0299", "tr12-0348", "tr12-0425", "tr12-0481", "tr12-0490", "tr12-0697", "tr13-0204", "tr13-0372", "tr14-0272", "tr14-0291"]
    train_seq = [os.path.basename(f) for f in glob.glob(FLAGS.data_dir + "/training_normalized_features/*")]
    if FLAGS.train_on_all == 0:
        train_seq = [b for b in train_seq if b not in test_seq]
    
    trainer = Trainer(FLAGS.data_dir, FLAGS.run_name, FLAGS.model, FLAGS.sleepstage_weight, FLAGS.draw_prc,
        train_sequences = train_seq, test_sequences=test_seq, draw_first_layer=FLAGS.draw_first_layer, probabilities_to_extreme=FLAGS.probabilities_to_extreme, post_scale_weight=FLAGS.post_scale_weight, post_offset_weight=FLAGS.post_offset_weight)
    trainer.run()
    # tf.app.run(main=trainer.run, argv=[sys.argv[0]] + unparsed)
