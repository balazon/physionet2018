from zipfile import ZipFile
import os
import glob

def package_entry(model = "models/test_gpu_1", checkpoint = "model-1000"):
    pattern_list = ["*.py",
                "train/*.py",
                "*.m",
                "*.sh",
                "*.vec",
                "readme.md",
                "{}/checkpoints/checkpoint".format(model),
                "{}/checkpoints/{}.data-00000-of-00001".format(model, checkpoint),
                "{}/checkpoints/{}.index".format(model, checkpoint),
                "{}/checkpoints/{}.meta".format(model, checkpoint),
                "{}/average_prc_test.txt".format(model),
                "{}/average_accuracy_test.txt".format(model),
                ]
    with ZipFile('entry.zip', 'w') as myzip:

        files = []
        for pattern in pattern_list:
            files += glob.glob(pattern)

        for _file in files:
            myzip.write(_file)

if __name__ == "__main__":
    package_entry()


