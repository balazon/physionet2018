<html>
<head>
<title>Writing a CinC Paper Using LaTeX</title>
</head>
<body bgcolor="white" text="#000000" link="#aa3333" vlink="#7d7d7f"
      alink="#000000">
<h1>Writing a CinC Paper Using LaTeX</h1>
<h3>George Moody &lt;george@mit.edu&gt;<br>Last revised: 20 February 2012</h3>

<p>
This directory contains the CinC Author's Kit for preparing papers in
PDF format using LaTeX.  The following files are included:

<table>
<tr><td><a href="README"><tt>README</tt></a></td><td>plain text version of this page</td></tr>
<tr><td><a href="balance.sty"><tt>balance.sty</tt></a></td><td>LaTeX macro for equalizing column length</td></tr>
<tr><td><a href="cinc.cls"><tt>cinc.cls</tt></a></td><td>CinC style file</td></tr>
<tr><td><a href="cinc.bst"><tt>cinc.bst</tt></a></td><td>CinC bibliography style file</td></tr>
<tr><td><a href="template.tex"><tt>template.tex</tt></a></td><td>generic template for a paper (LaTeX source)</td></tr>
<tr><td><a href="template.pdf"><tt>template.pdf</tt></a></td><td>formatted version of <tt>template.tex</tt></td></tr>
<tr><td><a href="refs.bib"><tt>refs.bib</tt></a></td><td>sample BibTeX bibliography file</td></tr>
<tr><td><a href="Makefile"><tt>Makefile</tt></a></td><td>rules for generating <tt>template.pdf</tt> using a <tt>make</tt> utility</td></tr>
<tr><td><a href="pdftex.cfg"><tt>pdftex.cfg</tt></a></td><td>needed if using an old (pre-2002)
version of <tt>pdflatex</tt> (see the note at the end of this file)</td></tr>
<tr><td><a href="example1/"><tt>example1</tt></a>, <a href="example2/"><tt>example2</tt></a></td><td>directories containing two
complete sample papers, including figures and bibliography files</td></tr>
<tr><td><a href="makebst/"><tt>makebst</tt></a></td><td>directory containing files needed to generate <tt>cinc.bst</tt></td></tr>
</table>

<p>
The entire kit may be downloaded as <a
href="../latex.tar.gz"><tt>latex.tar.gz</tt></a> or <a
href="../latex.zip"><tt>latex.zip</tt></a>.

<h2>INSTALLATION</h2>

<p>
This kit can be used with any reasonably modern (September, 1994 or
later) version of LaTeX, a dialect of Donald Knuth's TeX software for
typesetting.

<p>
If you don't already have LaTeX on your computer, download and install it now.
We recommend these TeX distributions:

<ul>
<li> <a href=http://www.tug.org/texlive/>TeX Live</a> for Linux or other Unix
<li> <a href=http://www.tug.org/mactex/>MacTeX</a> for Mac OS X
<li> <a href=http://www.tug.org/protext/>proTeXt</a> for MS Windows
</ul>

<p>
All are free and include everything needed other than the CinC-specific files
included in this author's kit.

<p>
To install the files provided in this directory, do one of the following:

<ul>
<li>Copy <tt>cinc.cls</tt> and <tt>cinc.bst</tt> to the directory that
 will contain your paper.</li>
<b>-- <em>or</em> --</b>
<li>Copy them to the standard locations for files of these types.  You may need
  to run <tt>texhash</tt> or <tt>mktexlsr</tt> afterwards, so that
  LaTeX and BibTeX can find these files.
</ul>

<p>
If your installation of LaTeX doesn't include <tt>balance.sty</tt>,
copy <tt>balance.sty</tt> from this directory into the same location
where you copied <tt>cinc.cls</tt>.

<p>
The instructions below assume that you will use <tt>pdflatex</tt> to
process your paper and to create a PDF file from it.  The recommended
TeX distributions mentioned above, and most others, include <tt>pdflatex</tt>.
<em>If you don't use pdflatex, use extra care to be sure that the page size
and margins in your final PDF are correct;  see the Appendix for details.</em>


<h2>FORMATTING THE EXAMPLES (OPTIONAL)</h2>

<p>
The use of these files is best illustrated by the included sample
papers (<tt>example1/example1.tex</tt> and
<tt>example2/example2.tex</tt>), which are accompanied by sets of PDF
(<tt>.pdf</tt>), PostScript (<tt>.ps</tt>), and encapsulated
PostScript (<tt>.eps</tt>) figures in <tt>example*/figures/</tt>, and
by BibTeX bibliography (<tt>.bib</tt>) files in example*/bib/, all of
which are referenced in the papers.  To format the sample papers:

<ol>
<li>Install <tt>cinc.cls</tt> and <tt>cinc.bst</tt> by copying them
   from this directory into the example directories, or into the
   standard locations.

<li>Enter the <tt>example1</tt> directory and do the following:
<pre>
	pdflatex example1   <i>[creates example1.aux, needed by bibtex]</i>
	bibtex example1	    <i>[creates example1.bbl, needed by pdflatex]</i>
	pdflatex example1   <i>[merges references]</i>
	pdflatex example1   <i>[produces final PDF with correct citation numbers]</i>
</pre>
</ol>

<p>
The multiple runs of <tt>pdflatex</tt> are needed to prepare data for
<tt>bibtex</tt> and then to resolve the cross-references.  These steps produce
<tt>example1.pdf</tt> (and several temporary files that can be removed).

<p>
The <tt>Makefile</tt> included in each directory can be used by a
<tt>make</tt> utility to automate these steps.

<p>
A similar procedure can be used to format <tt>example2.tex</tt>, which
illustrates other features of the CinC style files.


<h2>WRITING YOUR PAPER</h2>

<p>
Make a copy of the generic template (<tt>template.tex</tt>).  The
instructions below assume that you have saved your copy of the
template as a file named <tt><em>paper</em>.tex</tt>, into which you can type
your paper.

<p>
Please use the example files provided as models for your own papers. 
All references should be entered in BibTeX (<tt>.bib</tt>) database
files (see the samples in the <tt>example*/bib</tt> directories).  In
order to conform to the layout instructions, please check your final
version against the <a href="../example_layout.pdf">example layout</a>
provided.  Please double-check that you have followed these rules:

<ol>
 <li>The title of the paper should not end with a "."

 <li>List the authors of the paper in the form:
<pre>
	Albert B Johnson, Charles X Anderson
</pre>
    Do not use "." after initials, and do not use the word "and" before the
    name of the last author. <b><em>Since CinC is an English-language
    publication, and to ensure that your paper will be indexed correctly,
    follow the English-language convention for name order: given name(s) first,
    family names (surnames) last.</em></b>

 <li>List the authors' institutions within the <tt>\author</tt> statement.  Use
    superscripted numerals to identify the affiliations of each author if
    necessary.  Use <tt>\\</tt> to force a line break if needed, and
    use <tt>\mbox{}\\</tt> to create a blank line between the authors' names
    and their institutions.  For example:

<pre>
     \author{Hans~A~Kestler$^{1,2}$, Friedhelm~Schwenker$^1$, G~Hafner$^1$,
      Vinzenz~Hombach$^2$, G{\"u}nter~Palm$^1$, Martin~H{\"o}her$^2$ \\
      \mbox{}\\
      $^1$Neural Information Processing, University of Ulm, Germany \\
      $^2$Medicine II -- Cardiology, University Hospital Ulm, Germany}
</pre>

 <li>Inside table environments, place the <tt>\caption</tt> at the
     beginning to insure that the table legend appears above the table.	
     Inside figure environments, the <tt>\caption</tt> goes at the end,
     so that it appears below the figure.	

 <li>To create two equal-length columns on the last page, first identify what
    portion of your paper would be included in the first column on the
    last page, then insert the command
<pre>
	\balance
</pre>
    anywhere within this portion of text, and reformat.  (This works because
    <tt>cinc.cls</tt> now includes the standard <tt>balance.sty</tt>
    package.  Thanks to Erik Bojorges for this suggestion.)
</ol>

<h2>FORMATTING YOUR PAPER</h2>

<p>
You must submit the LaTeX source for your paper together with any other files
needed to produce a copy of your paper, such as files containing figures,
tables, and references.  Collect them together in a zip file to upload to
the paper collection site.

<p>
Before submitting the zip file, test that it is complete and verify that the
files within it produce a properly formatted paper of no more than 4 pages.
Do this by making a test PDF from the contents of the zip file.

<p>
There are several ways to generate a PDF from LaTeX sources; the
recommended method is described below.  The instructions below assume
that you have composed your paper in a file
called <tt><em>paper</em>.tex</tt> (substitute the name of your file
in the commands below, omitting the <tt>.tex</tt> suffix).

<ol>
 <li>Run these commands:
<pre>
	pdflatex <em>paper</em>
	bibtex <em>paper</em>
	pdflatex <em>paper</em>
	pdflatex <em>paper</em>
</pre>

 <li>If there were any errors or warnings, make any necessary corrections
     and repeat step 1.  Use a PDF viewer (such as Acrobat, xpdf or a
     recent copy of GhostScript) to view <tt><em>paper</em>.pdf</tt>, and
     correct any formatting errors.
</ol>

<p>
If you cannot use <tt>pdflatex</tt>, an alternative method is described in the
Appendix.

<h2>SUBMITTING YOUR PAPER</h2>

<p>
You must submit your paper electronically (paper copies are no longer
necessary or accepted).  Instructions on submitting your paper
using your web browser are included in the CinC Author's Kit.

<p>
The Editor will be available during the poster session to answer questions and
give advice. It would be helpful if you bring a single printed copy of your
paper with you, which the Editor may mark and will give back to you.


<h2>PRINTING YOUR TEST PDF</h2>

<p>
Most PDF viewers can also print PDF files.  If you use Adobe Acrobat,
Distiller, or Reader, choose <tt>Page Scaling: None</tt> and uncheck <tt>Auto
Rotate and Center</tt> in the <tt>Print</tt> dialog; this ensures that your
paper will be printed at the same scale and with the same top and left margins
as will be used in the published Proceedings.  Copies printed on A4 paper will
not be centered on the page, but they should be readable.

<p>
You may use color in figures, but print your paper on a black-and-white
printer and be sure that your figures are still legible.  The proceedings
are published in printed form (black-and-white only) as well as on CD-ROMs and
on-line (in color).  It's important that readers of the printed proceedings be
able to see your figures as you intended.

<hr>



<h2>ACKNOWLEDGEMENTS</h2>

<p>
The <tt>cinc.cls</tt> file was originally adapted by Hans Kestler from
Peter Nuchter's <tt>IEEEtran2e.cls</tt>, which in turn was adapted from
<tt>IEEEtran.sty</tt> by Gerry Murray and Silvano Balemi.  Bob Throne,
Alan Murray, Andrew Sims, George Moody, and Erik Bojorges contributed
feedback, bug fixes, and further improvements.  The <tt>cinc.bst</tt> file was
created by Hans Kestler and George Moody using Patrick Daly's <tt>makebst</tt>
generator.  George Moody wrote the generic template.

<p>
Your comments, suggestions, questions, and bug reports are welcome;  please
send them to George Moody (george@mit.edu).

<hr>

<h2>APPENDIX</h2>

<h3>Using <tt>latex</tt> instead of <tt>pdflatex</tt></h3>

<p>
Use this method only if you cannot use <tt>pdflatex</tt>.

<p>
If you have PostScript figures, note that you can still use <tt>pdflatex</tt>
if you first convert them to PDF using <tt>epstopdf</tt>, which is included
in your TeX distribution.  Use a command such as:

<pre>
        epstopdf <em>figure</em>.ps
</pre>

which creates <tt><em>figure</em>.pdf</tt>.

<p>
<em>To make an acceptable PDF without using <tt>pdflatex</tt>, it is very
important to follow the instructions below precisely.</em>  In these
instructions, note that "<tt>\</tt>" at the end of a line indicates that the
command continues on the next line.   The instructions below assume that
you have composed your paper in a file called <tt><em>paper</em>.tex</tt>
(substitute the name of your file in the commands below, omitting the
<tt>.tex</tt> suffix).

<ol>

<li>  Run these commands:
<pre>
	latex <em>paper</em>
	bibtex <em>paper</em>
	latex <em>paper</em>
	latex <em>paper</em>
</pre>

<li> You will now have a .dvi file named <tt><em>paper</em>.dvi</tt>.

<li> Run these commands:
<pre>
	dvips -Ppdf -t letter -o <em>paper</em>.ps <em>paper</em>.dvi
	ps2pdf -dPDFSETTINGS=/printer -dCompatibilityLevel=1.3 \
	 -dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true \
	 -sPAPERSIZE=letter <em>paper</em>.ps
</pre>
      <b>VERY IMPORTANT:</b> Use "<tt>letter</tt>" in the <tt>dvips</tt> and
      <tt>ps2pdf</tt> commands even if your normal paper size is A4, since the
      Proceedings are printed on US letter-sized pages.

<li>  If there were any errors or warnings, make any necessary corrections
      and repeat step 1.  Use Acrobat, xpdf, or GhostScript to view
      <tt><em>paper</em>.pdf</tt>, and correct any formatting errors.
</ol>


<h3>Using (very) old versions of <tt>pdflatex</tt></h3>

<p>
If you have installed one of the recommended TeX distributions mentioned
above, this section does not apply to you.

<p>
If you use a version of <tt>pdflatex</tt> older than 1.10 (released in 2002),
you may need to copy <tt>pdftex.cfg</tt> from this directory into the directory
that contains your paper before formatting it with <tt>pdflatex</tt>.
Versions 1.10 and later can get the information contained in <tt>pdtex.cfg</tt>
from <tt>\pdf...</tt> declarations in <tt>cinc.cls</tt>, and versions 1.20a and
later completely ignore <tt>pdftex.cfg</tt>.  The settings in question define
the paper size (8.5 x 11 inches, the size used to print the proceedings) and
the location of the coordinate system origin on the page.  If your
<tt>pdflatex</tt> needs <tt>pdftex.cfg</tt> and can't find it, your paper size
and margins may be incorrect.  If you are in doubt, open your PDF file with
GhostScript, xpdf, or Adobe Reader and check the paper size, which should be
8.5 x 11 inches (216 x 279 mm), not 210 x 297 mm as for A4 paper.
