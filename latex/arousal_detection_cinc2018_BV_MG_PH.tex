% -*- Mode:TeX -*-
% LaTeX template for CinC papers                   v 1.1a 22 August 2010
%
% To use this template successfully, you must have downloaded and unpacked:
%       http://www.cinc.org/authors_kit/papers/latex.tar.gz
% or the same package in zip format:
%       http://www.cinc.org/authors_kit/papers/latex.zip
% See the README included in this package for instructions.
%
% If you have questions, comments or suggestions about this file, please
% send me a note!  George Moody (george@mit.edu)
%
\documentclass[twocolumn]{cinc}
\usepackage{graphicx}
\hyphenpenalty=2000
\begin{document}
\bibliographystyle{cinc}

% Keep the title short enough to fit on a single line if possible.
% Don't end it with a full stop (period).  Don't use ALL CAPS.
\title{Using Auxiliary Loss to Improve Sleep Arousal \\
	Detection with Neural Network
}

\author {  B\'{a}lint Varga, M\'{a}rton G\"{o}r\"{o}g, P\'{e}ter Hajas \\
\ \\ % leave an empty line between authors and affiliation
}

\maketitle

% LaTeX inserts the ``Abstract'' heading in the proper style and
% sets the text of the abstract in italics as required.
\begin{abstract}

Our pipeline consists of a hand-crafted preprocessor and a neural network classifier. We applied linear and nonlinear transformations on the physiologic signals in order to gain features in both time- and frequency domains. The proposed algorithm was trained on 994 manually annotated records of polysomnographic signals. Most of the features were generated from the EEG signal such as power spectral density, and entropy. We extracted features from the EOG, EMG, and airflow signals too. The heart rate (BPM) feature was generated from the ECG signal using peak detection. All the features were normalized.

These 68 features were resampled in 21 non-continuous (past and future) moments around the current timestamp, and fed into a 3-layer neural network in order to assign a probability of arousal at each second. A 1D layer extracted tendencies, while 2 fully connected layers learned combinations and probabilities. Arousal samples were enriched during training, as they consist of a small part of the data. Additional (‘auxiliary’) losses can guide the network to learn high-level concepts, even though they will not be evaluated. We used sleep stages as additional training targets, which were easier to learn than arousals despite being multi-class. This approach slightly increased arousal AUPRC.

Our submitted results for the entire test set were evaluated: AUPRC=0.42.

Our 10-fold cross validation results for the AUPRC are the following: 
[0.47110,
0.41672,
0.44305,
0.42842,
0.44644,
0.47969,
0.45082,
0.49320,
0.45913,
0.41278] averaging 0.450.

\end{abstract}
% LaTeX inserts the extra space here automatically.

\section{Introduction}

As a contribution to the Physionet/Computing in Cardiology Challenge 2018 \cite{PhysioNet_challenge} this paper focuses on the  design and implementation of an algorithm which is capable of detecting sleep arousals from polysomnographic signals.

We chose to solve the task with a neural network which can classify each time-point in the input data as either an arousal or a non-arousal region. Instead of feeding the raw 200 Hz polysomnographic data directly to the network our pipeline first preprocesses these signals using domain knowledge henceforth referred to as features.
Since the length of the annotated arousal regions are in the magnitude of seconds we chose a resolution of 1 Hz for these intermediate features. We used supervised learning on these feature signals to train a relatively small neural network.

\begin{figure*}
	\centering 
	\includegraphics{network_architecture.pdf}
	\caption{Network architecture.}
	\label{FIG_network_architecture}
\end{figure*}

\section{Feature extraction}

The EEG (electroencephalogram) signals were first analysed in the frequency domain since each sleep stage is characterized by a specific frequency band \cite{EEG_freq_bands}. Five features were extracted from each EEG channel in the traditional frequency bands \cite{EEG_freq_bands} with the help of the Welch method \cite{EEG_Welch} in 9 second length windows. Initially we used 3 second length windows but the results showed that the classifier is more stable with a longer window selection.

Approximate Entropy (ApEn) was applied to the EEG signals based on the work in \cite{EEG_ApEn} with the suggested parameters. It was developed as a measure of system complexity \cite{Pincus_ApEn}. A high value of ApEn indicates random and unpredictable variation, whereas a low value of ApEn indicates regularity and predictability in a time series \cite{Pincus_ApEn}.

We used the SaO2 level mostly untouched: we rescaled it between 60\% and 100\% in order to ensure that it reflects to the physiologically relevant range.

We extracted several features from the EOG (electrooculogram) signal based on the results in \cite{EOG_features}. In this publication time- and frequency domain features were applied to the 5 second length windows of the digitized EOG data. Their goal was to detect REM sleep stage with appropriately designed features fed to a neural network. In order to obtain higher classification accuracy they applied Sequential Backward Selection on the features. Based on their results we chose six features, namely the form factor, standard deviation, skewness, kurtosis, and the relative energies in two regions: 0 Hz -- 2 Hz and 2 Hz -- 4 Hz. These statistics were also applied to the abdomen and chin EMG (electromyogram) signals.

The respiratory signal gives valuable information to arousal detection. In \cite{Airflow_envelope} an envelope related signal was evaluated: the Respiratory Disturbance Variable (RDV). As suggested, we processed the previously band-pass filtered signals applying 30 second length windows in 10 second steps. The envelope was obtained by Hilbert Transform. The RDV of the given window was calculated as the ratio of the standard deviation and the mean of the envelope with the application of a correction factor \cite{Airflow_envelope}.

We applied Homomorphic Envelope as in \cite{Springer_Homomorphic} for some of the signals such as the EEG channels, abdomen and chin EMGs.

The ECG (electrocardiogram) signal was transformed into a heart rate feature following the basic principles in \cite{hamilton2002open}. The QRS peak detector was modified to filter peak candidates even further by removing peaks whose amplitude did not reach a ratio of a max-pooled value. (A max-pool window of 5 seconds, and a ratio of 0.4 was used).

\subsection{Time-slot extraction} 

Instead of taking continuous chunks of the features, and feeding it to the network, a time-slot extraction is performed first. This was driven by the intuition that we needed a large receptive field. We also wanted to keep the computational and memory costs low, so we avoided dilated convolutions. For this purpose we resampled our 1 Hz feature signals around the examined timestamp in 21 time-slots and continuously concatenated the samples. The sampling is denser at the center and gets gradually sparser at the edges. The whole range of the time-slots provides input from a 2-minute window of the features.

\section{Neural network training}

\subsection{Network structure}

The neural network depicted in \textit{Figure \ref{FIG_network_architecture}.} takes a 21 x 68 x 1 (HWC) sized tensor as input, consisting of the timeslot extracted features where H = number of time-slots = 21 and W = number of features = 68. Then a 2D convolution is run with a kernel size of 21 x 1, a filter size of 32, and stride of 21 x 1, resulting in a 1 x 68 x 32 shaped tensor. This tensor is further processed by a fully connected layer with 128 outputs, and finally another fully connected layer with 7 outputs. These 7 outputs make up 2 logits of arousal classification, and 5 logits of sleep stage classification.

We used cross-entropy loss on the classification outputs (one for arousals, and another for the sleep stages), and weighted the losses to balance between the arousal and sleep stage error.

\subsection{Auxiliary loss}

Unlike some other machine learning methods, neural networks are flexible enough to be able to learn multiple tasks on the same input data at the same time. A major part of the network is shared, only the last layers are separated into independent heads trained with separate objectives. A detailed introduction can be found in \cite{ruder_multitask}.

In case one needs to predict multiple targets (like age and sex of a person), and both outputs are necessary, this method is called multi task learning. One can save resources by uniting the two classifiers.

However, if we actually care about only one class of the results (only the sex), but we have additional information which is not provided as an input to the network (age), we may also require the model to output this information, as we actually provide further guidance through the additional loss to the learner (auxiliary loss). Although we will not use this output at inference time, the richer training signal can help build a better representation, and therefore also improve accuracy on the main task.

As the annotations contained sleep-stages, we used this classification as an auxiliary loss. Using it as the network input was not an option, as sleep-stages were not given to the test set.

\subsection{Data filtering, data imbalance}

The extracted 1 Hz features were slightly preprocessed before being used as training data.

All features were normalized with their mean absolute value to balance patient-differences. We wanted to detect changes during a patient's sleep, and ignore differences between patients. The resulting data was clipped to [-100, 100] to cut outliers which could damage the weights of the neural network. Both normalizing and clipping resulted in better accuracy.

Moments with invalid arousal annotations were skipped, as no evaluation could be made with those. However, moments with invalid sleep stage-annotation were kept.

The data naturally contained many more samples without arousal than with it, which needs to be handled to avoid losing precision. We implemented a data selector, which ensured that at least 25\% of the data fed to the network is arousal data, by dropping non-arousal moments when constructing batches.

\section{Discussion}
\subsection{Batch size}

\vspace{-4 mm}
\begin{table}[htbp]
	\caption{Batch size effect on AUPRC result.}
	\vspace{4 mm}
	\centerline{\begin{tabular}{|c||c|c|c|c|} \hline
			Batch size  & 4  & 10 & 20 & 40\\ \hline
			AUPRC       & 0.42549 & 0.44509 & 0.45586 & 0.45969 \\ \hline
		\end{tabular}}
	\end{table}

These tests were run for 50k training iterations, measured on a fixed set of training sequences (validation set) previously unseen by the network. As seen by the table, a batch size of 40 yielded the best results. It is possible that larger batch sizes may provide even further performance benefits, but they are much slower to train, so we kept our batch size at 20 to keep training times reasonable.


\subsection{ Auxiliary loss weight}   

Experiments were made to find the best weight of the auxiliary loss. The results are noisy on shorter trainings, but we can conclude that AUPRC deteriorates above 0.5 weight, but may be helpful below (\textit{Figure \ref{FIG_arousal_weights}.}).


\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{weights2.pdf}
	%\includegraphics[width=7.9cm]{arousal_weight.png}
	\caption{The Effect of the Weight of the Auxiliary Loss.}
	\label{FIG_arousal_weights}
\end{figure}

Additionally, an experiment was made to drop the auxiliary loss during the last 10\% of training, when inner representation is constructed, but final weights could support the major target. We measured worse prediction accuracy; therefore, this idea was not used.

\subsection{Representation within the neural network}

Visualizing weights of the neural network in \textit{Figure \ref{FIG_filter_coeffs}.} reveals the learned filters. Each column shows weights of a time-slice, while each row is a separate filter. We can find filters which are sensitive to decreasing values in time (first 2 rows). Other mark moments where past and future have higher values and we are at local minimum (8th), one sees a (low) peak in the close future (9th) or simply calculates the average (=sensitive to data with low variance) (18th).

\begin{figure}[h]
	\centering
	\includegraphics[width=\columnwidth]{filter.pdf}
	\caption{Input filter representation.}
	\label{FIG_filter_coeffs}
\end{figure}

This diversity shows rich building blocks for higher layers. Using a higher number of filters did not help training. Higher level weights are not shown as in a fully-connected network they are not easy to interpret.

\subsection{Dropout}

We have experimented with dropout but found that it did not increase performance. Perhaps when training for more iterations or with a different architecture results would be different.

\subsection{Postprocess}

Several methods were tried which modified the output arousal probabilities, mostly building on the observation that arousals are annotated in 10 -- 30 seconds long blocks. While it seems logical that a lot of arousal-classified samples (in our case the unit was 1 second) reinforce the adjacent lower probability moments, these experiments did not give reliably better results; therefore, they were not used in the final version.

\section{Conclusion}

We have presented an arousal detector algorithm for polysomnographic data with relatively low resource needs. As the first step, we generate hand-crafted intermediate features using background knowledge. Using this transformed data, a neural network classifies arousal / non-arousal moments.

The evaluation allowed us to classify the data offline --- that is after the recording has finished ---, however the classifier could easily be modified to work near real-time if a use-case requires it: only the normalization and time-slots would need slight modifications.


\balance

% This section is not numbered.
% 

% LateX generates the ``References'' heading automatically and switches
% to 9 point type for the bibliography.  If you use BibTeX (recommended),
% follow the examples in the sample 'refs.bib' file to enter your references,
% and leave the following line unchanged.
\bibliography{refs}

\end{document}


% % Remove the '%' from the previous line before formatting the final
% % version of your paper
% 
% \clearpage
% \setcounter{section}{-1}
% 
% \section{Instructions for preparing your paper}
% 
% \subsection{This document}
% 
% Please save this document and use the outline on the first page to
% prepare your paper.
% 
% Before formatting your paper, remove these
% instructions by uncommenting the \verb+\end{document}+ line that you
% will find in this document a few lines above this paragraph.
% 
% \subsection{Paper length}
% 
% Please limit your paper to no more than four pages.
% 
% \subsection{LaTeX formatting}
% 
% In order to use this template to create a paper in proper CinC
% style, you will also need to have the CinC LaTeX macros, which
% can be downloaded from http://www.cinc.org/authors\_kit/ in either
% .tar.gz or .zip format.  These files include instructions on
% how to use them, together with complete example papers
% illustrating how to include equations, figures, tables, and
% references.
% 
% \subsection{Title}
% 
% Avoid abbreviations and keep to one or two lines. Remember that the
% title should be easily understood when cited as a reference in another
% publication.
% 
% \balance % equalize column lengths -- see comments on final page below
% 
% \subsection{Section numbering}
% 
% Number your sections as illustrated, \emph{starting with 1.}
% % LaTeX does this automatically.
% 
% 
% \subsection{Tables and figures}
% 
% Tables and figures can fit across both columns if necessary.  Captions go
% above tables, but beneath figures.
% 
% 
% 
% 
% \subsection{Citations and references}
% 
% All references should be included in the text in square brackets in
% their order of appearance, e.g. [1] [1,2] [1--4]. In the reference list
% use the Vancouver style (see BMJ 1991;302:338--41 or New Engl J Med
% 1991;324:424--8).
% 
% References to Computers in Cardiology proceedings should now include
% the volume number. For volumes before 1997, set out as for book
% chapters giving publisher and place of publication, noting that the
% publisher changed from IEEE Computer Society Press to IEEE in 1995.
% 
% There are (at least) two ways to prepare references.  We recommend
% that you use BibTeX if you can, since it automatically numbers your
% citations and generates a properly sorted reference list in
% CinC format, complete with a section heading.
% 
% If you do not use BibTeX, prepare your list of references following
% the commented-out examples on the first page of this document.
% 
% 
% \subsection{Final page}
% 
% The text on the final page should be arranged so that both columns
% are approximately the same height.   Insert \verb+\balance+ anywhere
% within the first column, and LaTeX will
% even out the columns automatically.
% 
% 
% \subsection{End of instructions}
% 
% \emph{All of section 0 should be deleted before you format the
% final version of your paper.}
% 
\end{document}
