### Python library for Physionet 2018 challenge ###

dependencies:
 - h5py
 - numpy
 - scipy
 - wfdb
 - tqdm
 - tensorflow

## For training:
Set the DATA_ROOT variable in main.py for your data location, by default it's the data folder in the current directory
`DATA_ROOT = "./data"`

The data should be in the following structure:
DATA_ROOT
└── training/
    ├── tr03-0005
    │   ├──tr03-0005.arousal
    │   ├──tr03-0005.hea
    │   ├──tr03-0005.mat
    │   └──tr03-0005-arousal.mat
    ├── tr03-0029
    └── ...

You need the feature files generated first from the raw dataset, before you can train:
..
TODO 



## For the inference pipeline:
Same as for the physionet python sample.
Call next.sh with the <record-name> as parameter, for eg.:
```next.sh te10-0041```

This will prepare our feature files first, then use a neural network to estimate the arousal probabilities, which are then dumped in <record_name>.vec

