from shutil import copyfile
import os
import numpy as np

def normalizeFeature(inputFile, outputFile):
    f = open(inputFile, 'rb')
    data = np.fromfile(f, dtype=np.float32)
    f.close()
    
    avg = np.mean(data)
    if np.max(data) > 10000 or np.min(data) < -10000:
        print(inputFile, " ", np.min(data), " ", np.max(data))
    if not np.isfinite(avg):
        print(inputFile, " ", avg)
    
    data -= avg
    maxx = np.mean(abs(data))
    if maxx == 0:
        maxx = 1
    data /= maxx
    data = np.clip(data, -100, 100)

    data.tofile(outputFile)

def normalizeFeatures(inputDir, outputDir):
    for dirName, subdirList, fileList in os.walk(inputDir):
        for fname in fileList:
            inputFile = '%s/%s' % (dirName, fname)
            outputFile = inputFile.replace(inputDir, outputDir)
            #print(inputFile, " " , outputFile)
            os.makedirs(os.path.dirname(os.path.realpath(outputFile)), exist_ok=True)
            if inputFile.endswith(".bin"):
                normalizeFeature(inputFile, outputFile)
            elif inputDir != outputDir:
                copyfile(inputFile, outputFile)

if __name__ == "__main__":
    DATA_DIR="/home/AD.ADASWORKS.COM/marton.gorog/Desktop/"
    NOTNORMALIZED_DIR=DATA_DIR + "/training_features"
    NORMALIZED_DIR=DATA_DIR + "/training_normalized_features"
    normalizeFeatures(NOTNORMALIZED_DIR, NORMALIZED_DIR)
