import glob
import sys
import os
import preprocess
import numpy as np
import tqdm

from train.train import Trainer

data_dir = '/media/marton.gorog/My Passport/Marci/Prog/Physionet2018/'
train_seq = [os.path.basename(f) for f in glob.glob(data_dir + "/test_features/*")]
print(train_seq)

tf_model = Trainer(data_dir=".", run_name = "longgg100k_v4_final", model = None)
out_dir = data_dir + os.path.sep + 'annotations'
os.makedirs(out_dir, exist_ok=True)
skip_existing=True

for t in tqdm.tqdm(train_seq):
    output_file = out_dir + os.path.sep + t + '.vec'
    if skip_existing and os.path.isfile(output_file):
        continue
    features_dir = data_dir + os.path.sep + "/test_features/" + t
    #preprocess.preprocess_record(record_name, '.', features_dir)

    tf_model.data_loader.init_testing(features_dir)
    tf_model.init_infer()
    arousal_probabilities = tf_model.infer(features_dir)

    np.savetxt(output_file, arousal_probabilities, fmt='%.3f')
    print("Saved {}".format(output_file))
