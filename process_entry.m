function process_entry(varargin)

	output_directory = 'features';
	switch(nargin)
		case 1
			entry_name = varargin{1};
		case 2
			entry_name = varargin{1};
			output_directory = varargin{2};
		otherwise
			throw('Insufficient number of arguments');
	end

	mkdir(output_directory);
	[tm, signal, fs, siginfo] = rdmat(entry_name);

	signal_names = cell(length(siginfo), 1);
	for i = 1 : length(siginfo)
		signal_names{i, 1} = siginfo(i).Description;
	end
	SaO2_ind = 12;%find(contains(signal_names,'SaO2'));
	AIRFLOW_ind = 11;%find(contains(signal_names,'AIRFLOW'));
	CHEST_ind = 10;%find(contains(signal_names,'CHEST'));
	ABD_ind = 9;%find(contains(signal_names,'ABD'));
	CHIN_ind = 8;%find(contains(signal_names,'Chin1-Chin2'));
	E1_M2_ind = 7;%find(contains(signal_names,'E1-M2'));

	SaO2 = signal(:, SaO2_ind)';
	airflow = signal(:, AIRFLOW_ind)';
	chest = signal(:, CHEST_ind)';
	abd = signal(:, ABD_ind)';
	chin = signal(:, CHIN_ind)';
	eog = signal(:, E1_M2_ind)';
	eeg = signal(:, 1 : 6)';

	clear signal;
	data_x = 0 : 1 : tm(end);

	if 1
 		% abd emg
 		[d, d_x] = generate_emg_features(abd, tm, fs);
 		for j = 1 : size(d, 1)
 			d_interp = interp_lin_with_ends(d_x, d(j, :), data_x);
 			processed_name = [output_directory, filesep, 'abd_stats_', num2str(j), '.bin'];
 			fileID = fopen(processed_name, 'w');
 			fwrite(fileID, d_interp, 'float');
 			fclose(fileID);
 		end
 
 		% chin emg
 		[d, d_x] = generate_emg_features(chin, tm, fs);
 		for j = 1 : size(d, 1)
 			d_interp = interp_lin_with_ends(d_x, d(j, :), data_x);
 			processed_name = [output_directory, filesep, 'chin_stats_', num2str(j), '.bin'];
 			fileID = fopen(processed_name, 'w');
 			fwrite(fileID, d_interp, 'float');
 			fclose(fileID);
 		end

		[b, a] = butter(3, [1, 40] / (fs / 2));
		abd = filtfilt(b, a, abd);
		chin = filtfilt(b, a, chin);

		lpf_frequency = 0.1;
		homomorphic_envelope = Homomorphic_Envelope_with_Hilbert(abd, fs, lpf_frequency);
		processed_name = [output_directory, filesep, 'abd_envelope_fltd', '.bin'];
		fileID = fopen(processed_name, 'w');
		homomorphic_envelope = interp_lin_with_ends(tm, homomorphic_envelope, data_x);
		fwrite(fileID, homomorphic_envelope, 'float');
		fclose(fileID);

		homomorphic_envelope = Homomorphic_Envelope_with_Hilbert(chin, fs, lpf_frequency);
		processed_name = [output_directory, filesep, 'chin_envelope_fltd', '.bin'];
		fileID = fopen(processed_name, 'w');
		homomorphic_envelope = interp_lin_with_ends(tm, homomorphic_envelope, data_x);
		fwrite(fileID, homomorphic_envelope, 'float');
		fclose(fileID);	    
	end

	if 1
		% Electrooculogram
		[d, d_x] = generate_emg_features(eog, tm, fs);
		for j = 1 : size(d, 1)
		  d_interp = interp_lin_with_ends(d_x, d(j, :), data_x);
		  processed_name = [output_directory, filesep, 'eog_stats_', num2str(j), '.bin'];
		  fileID = fopen(processed_name, 'w');
		  fwrite(fileID, d_interp, 'float');
		  fclose(fileID);
		end
	end

	if 1
 		% EEG envelope
 		lpf_frequency = 0.1;
 		for k = 1 : 6
 			homomorphic_envelope = Homomorphic_Envelope_with_Hilbert(eeg(k, :), fs, lpf_frequency);
 			processed_name = [output_directory, filesep, 'eeg_noise_', num2str(k), '_.bin'];
 			fileID = fopen(processed_name, 'w');
 			homomorphic_envelope = interp_lin_with_ends(tm, homomorphic_envelope, data_x);
 			fwrite(fileID, homomorphic_envelope, 'float');
 			fclose(fileID);	
 		end

		% APEN features
		[e, e_x] = generate_apen_eeg_features(eeg, fs, tm);
		for k = 1 : 6
			di = interp_lin_with_ends(e_x, e(k, :), data_x);
			processed_name = [output_directory, filesep, 'eeig_apen_', num2str(k), '_.bin'];
			fileID = fopen(processed_name, 'w');
			fwrite(fileID, di, 'float');
			fclose(fileID);	
		end

 		% SaO2
 		SaO2_feature = generate_SaO2_feature(SaO2);
 		data = interp1(tm, SaO2_feature, data_x, 'nearest');
 		processed_name = [output_directory, filesep, 'SaO2.bin'];
 		fileID = fopen(processed_name, 'w');
 		fwrite(fileID, data, 'float');
 		fclose(fileID);	
	end

	if 1
		[b, a] = butter(3, [0.1, 2] / (fs / 2));
		airflow_bw = filtfilt(b, a, airflow);
		[RDV_x, RDV, MA] = generate_rdv_feature(airflow_bw, fs, tm);
		data = interp_lin_with_ends(RDV_x, RDV, data_x);
		processed_name = [output_directory, filesep, 'airflow.bin'];
		fileID = fopen(processed_name, 'w');
		fwrite(fileID, data, 'float');
		fclose(fileID);
		data = interp_lin_with_ends(RDV_x, MA, data_x);
		processed_name = [output_directory, filesep, 'airflow_mean_amp.bin'];
		fileID = fopen(processed_name, 'w');
		fwrite(fileID, data, 'float');
		fclose(fileID);

		[b, a] = butter(3, [0.05, 1] / (fs / 2));
		chest_bw = filtfilt(b, a, chest);
		[RDV_x, RDV, MA] = generate_rdv_feature(chest_bw, fs, tm);
		data = interp_lin_with_ends(RDV_x, RDV, data_x);
		processed_name = [output_directory, filesep, 'chest.bin'];
		fileID = fopen(processed_name, 'w');
		fwrite(fileID, data, 'float');
		fclose(fileID);
		data = interp_lin_with_ends(RDV_x, MA, data_x);
		processed_name = [output_directory, filesep, 'chest_mean_amp.bin'];
		fileID = fopen(processed_name, 'w');
		fwrite(fileID, data, 'float');
		fclose(fileID);
	end
end

function SaO2_feature = generate_SaO2_feature(SaO2)
	SaO2 = max(SaO2 - 60, 0);
	SaO2_feature = min(max(SaO2 / 20 - 1, -1), 1);
end

function [RDV_x, RDV, mean_amp] = generate_rdv_feature(input, fs, ts)
	% https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3902884/
	% Díaz JA, Arancibia JM, Bassi A, Vivaldi EA. 
	% Envelope Analysis of the Airflow Signal To Improve Polysomnographic Assessment of Sleep Disordered Breathing. 
	% Sleep. 2014;37(1):199-208. doi:10.5665/sleep.3338.
	envelope = abs(hilbert(input));
	siglen_smp = length(input);
	winlen_smp = 30 * fs;
	shift_smp = 5 * fs;
	[ibegin, iend] = define_epochs(siglen_smp, winlen_smp, shift_smp);
	nwin = length(ibegin);
	RDV_x = ((ts(ibegin) + ts(iend)) ./ 2);
	RDV = zeros(1, nwin);
	mean_amp = zeros(1, nwin);
	for ii = 1 : nwin
		i1 = ibegin(ii);
		i2 = iend(ii);
		p = envelope(i1 : i2);
		m = mean(p);
		RDV(ii) = std(p) / m / 0.523;
		mean_amp(ii) = m;
	end
	RDV(isnan(RDV)) = 0;
end

function [data, data_x] = generate_emg_features(sig, ts, fs)
	% Coskun, Ahmet & Özşen, Seral & Yücelbaş, Şule & Yücelbaş, Cüneyt & Tezel, Gulay & Kuccukturk, Serkan & Yosunkaya, Sebnem. (2016). 
	% Detection of REM in Sleep EOG Signals. Indian Journal of Science and Technology. 9. 10.17485/ijst/2016/v9i25/96631. 
	sig = sig - mean(sig);
	[b, a] = butter(3, [0.05, 35] / (fs / 2));
	sig = filtfilt(b, a, sig);
	sig = sig / 1000;
	siglen_smp = length(sig);
	winlen_smp = 5 * fs;
	shift_smp = 5 * fs;
	[ibegin, iend] = define_epochs(siglen_smp, winlen_smp, shift_smp);
	nwin = length(ibegin);
	data_x = ((ts(ibegin) + ts(iend)) ./ 2);
	data = zeros(5, nwin);
	fwin = blackman(winlen_smp);
	
	SF = zeros(1, nwin);
	xstd = SF;
	xskw = SF;
	xkrt = SF;
	energy_0_2 = SF;
	energy_2_4 = SF;
	
	for ii = 1 : nwin
		i1 = ibegin(ii);
		i2 = iend(ii);
		p = sig(i1 : i2);

		n = length(p);
		xrms = sqrt(sum(p .* p) / n);
		SF(ii) = xrms / (sum(sqrt(abs(p))) / n);
		xstd(ii) = std(p);
		xskw(ii) = skewness(p);
		xkrt(ii) = kurtosis(p);

		sfft = fft(p .* fwin');
		sfft = abs(sfft(1 : end/2));
		len_2Hz = length(sfft) / (fs / 2) * 2;
		energy_0_2(ii) = sum(sfft(1 : len_2Hz));
		energy_2_4(ii) = sum(sfft(len_2Hz + 1 : 2 * len_2Hz));
	end
	data(1, :) = SF;
	data(2, :) = xstd;
	data(3, :) = xskw;
	data(4, :) = xkrt;
	data(5, :) = energy_0_2;
	data(6, :) = energy_2_4;
	data(isnan(data)) = 0;
end

function d_interp = interp_lin_with_ends(d_x, d, data_x)
  	d_interp = interp1(d_x, d, data_x, 'linear');
    in = isnan(d_interp);
    in(round(end / 2) : end) = false;
    d_interp(in) = d(1);
    in = isnan(d_interp);
    in(1 : round(end / 2)) = false;
    d_interp(in) = d(end);
end

function [data, data_x] = generate_apen_eeg_features(eeg, fs, ts)
	% Burioka N, Miyata M, Corn�lissen G, et al. 
	% Approximate Entropy in the Electroencephalogram During Wake and Sleep. 
	% Clinical EEG and neuroscience?: official journal of the EEG and Clinical Neuroscience Society (ENCS). 2005;36(1):21-24.
	[nchannels, siglen_smp] = size(eeg);
	winlen_smp = 5 * fs;
	shift_smp = 5 * fs;
	[ibegin, iend] = define_epochs(siglen_smp, winlen_smp, shift_smp);
	nwin = length(ibegin);
	data_x = ((ts(ibegin) + ts(iend)) ./ 2);
	data = zeros(nchannels, nwin);
	
	[b, a] = butter(3, [0.5, 32] / (fs / 2));
	m = 2;
	for ch = 1 : nchannels
		signal = filtfilt(b, a, eeg(ch, :));
		for ii = 1 : nwin
			i1 = ibegin(ii);
			i2 = iend(ii);
			p = signal(i1 : i2);
			r = 0.2 * std(p);
			data(ch, ii) = apen(p, m, r);
		end
	end
end

function ae = apen(signal, m, r)
	% http://physionet.caregroup.harvard.edu/physiotools/ApEn/
	% https://en.wikipedia.org/wiki/Approximate_entropy
	% Contains ideas from the following implementation: 
	% https://www.mathworks.com/matlabcentral/fileexchange/32427-fast-approximate-entropy
	N = length(signal);
	sp = zeros(2, 1);
	ds = 10;
	for j = 1 : 2 
		m_ = m + j - 1;
		Xidx = hankel(1 : m_, m_ : N);
		Xidx = Xidx(:, 1 : ds : N);
		sequences = signal(Xidx);
		ncols = size(sequences, 2);
		phi = zeros(1, ncols);
		for i = 1 : ncols
			tmp1 = abs(sequences - repmat(sequences(:, i), 1, ncols));
			tmp2 = any((tmp1 > r), 1);
			phi(i) = sum(~tmp2);
		end
		sp(j) = mean(phi) / ncols;
	end
	ae = log(sp(1) / sp(2));
end

function [ibegin, iend] = define_epochs(siglen_smp, winlen_smp, shift_smp)
	n_wins = floor((siglen_smp - (winlen_smp - shift_smp + 1)) / shift_smp);
	ibegin = 1 : shift_smp : 1 + shift_smp * (n_wins - 1);
	iend = ibegin + winlen_smp - 1;
end
