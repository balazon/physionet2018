import os
import wfdb

from ecg import processECG
from eeg import processEEG
from normalize_features import normalizeFeatures

# generate features
def preprocess_record(record_name, path, output_dir):
    os.makedirs(output_dir, exist_ok = True)
    
    header = wfdb.rdheader(path + "/" + record_name)
    # print("info: length of {}: {:.1f} hours".format(basename, header.sig_len/200.0/3600.0))

    # Save length
    f = open(output_dir + '/sample_count.txt', 'w')
    print(header.sig_len, file=f)
    f.close()

    # signal = wfdb.rdrecord(path + "/" + record_name)
    sig, fields = wfdb.rdsamp(path + "/" + record_name)
    #signal.p_signal is the same as sig, signal has some additional data (things that can be found in fields, and maybe more)
    sig = sig.transpose()

    # Process ECG
    ecg_index = fields['sig_name'].index('ECG')
    processECG(sig[ecg_index], record_name, output_dir)

    # Process EEG
    for i in range(6):
        processEEG(sig[i], record_name, fields['sig_name'][i], output_dir)
    
    normalizeFeatures(output_dir, output_dir) # normalizing in-place
