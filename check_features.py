import numpy as np
import matplotlib.pyplot as plt
import os

def displayFeatures(path_list):
    draw = False
    if draw:
        plt.figure(1)
        plt.margins(0, 0, tight=True)
    if len(path_list) > 200:
        path_list = path_list[0:100]
    if draw:
        ax = None
    for i, path in enumerate(path_list):
        if draw:
            ax = plt.subplot(len(path_list)+1, 1, i+1, sharex=ax)
            plt.title(os.path.basename(path))
        f = open(path, 'rb')
        data = np.fromfile(f)
        if np.max(data) > 5:
            print(path, np.max(data))
        if np.min(data) < -5:
            print(path, np.min(data))
        if np.any(np.isnan(data)):
            print(path, "nan ", np.sum(np.isnan(data)))
        if draw:
            ax.plot(data)
        f.close()
    
    if draw:
        plt.show()
