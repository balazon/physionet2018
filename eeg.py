import matplotlib.pyplot as plt
import scipy.signal as signal
import numpy as np
import time
import tqdm
import os

plot_datas = []
def addPlot(data):
    plot_datas.append(data)

def clearPlots():
    plot_datas.clear()
    

def showPlots():
    num_plots = len(plot_datas)
    for idx, data in enumerate(plot_datas):
        plt.subplot(num_plots, 1, idx + 1)
        plt.plot(np.arange(0, len(data)), data, 'r-')
    plt.show()


def findPeaks(data : np.ndarray):
    peaks = np.zeros(data.shape)
    peakidx = np.array([], dtype=np.int64)

    # find initial peaks
    for idx, val in enumerate(data):
        #ignore first and last
        if idx in [0, len(data) - 1]:
            continue
        # print(idx)
        if (max(data[idx - 1], data[idx + 1]) < val):
            peaks[idx] = 1
            peakidx = np.append(peakidx, idx)

    # filter by threshold
    for i in range(len(data) // 400 + 1):
        if i == len(data) // 400:
            peaksub = peaks[-400:]
            datasub = data[-400:]
        else:
            peaksub = peaks[i * 400 : (i + 1) * 400]
            datasub = data[i * 400 : (i + 1) * 400]
        thresh = np.max(datasub) * 0.4
        peaksub[datasub < thresh] = 0

    peakidx = np.where(peaks > 0)[0]


    # suppress peaks that have larger peaks neighboring them (less than 196 ms)
    for idx, p in enumerate(peakidx):
        windowHalf = 196 // 5
        leftEnd, rightEnd = False, False
        for i in range(1, windowHalf * 2 + 1):
            if leftEnd and rightEnd:
                break
            j = i // 2 + 1 if i % 2 == 1 else -i // 2
            if idx + j < 0:
                leftEnd = True
                continue
            if idx + j > len(peakidx) - 1:
                rightEnd = True
                continue
            j = peakidx[idx + j]
            if j < max(0, p - windowHalf):
                leftEnd = True
                continue
            if j > min(peakidx[-1], p + windowHalf):
                rightEnd = True
                continue
            if data[j] > data[p]:
                peaks[p] = 0
                break
    
    return peaks

def preprocess(data):
    # band pass filter
    b1 = signal.firwin(34, [0.001, 0.25, 0.35, 0.49], pass_zero=False, nyq=0.5)
    b1 = signal.firwin(34, [0.025, 0.13], pass_zero=False, nyq=0.5)
    
    b2 = signal.firwin(34, [0.001, 0.15], pass_zero=False, nyq=0.5)

    # filtered1 = signal.filtfilt(b1, [1.0], ecg)
    filtered2 = signal.filtfilt(b2, [1.0], data)
    return filtered2

def processEEG(eeg, patient_id, signal_name, output_dir):
    halfwindow = 9*200
    draw = False
    stepsize = 200
    do_scikit_welch = True
    do_plt_psd = False
    normalize = True
    skip_if_exists = True
    # features_dir = input_dir.replace("training", "training_features").replace("test", "test_features")

    if skip_if_exists and os.path.isfile(output_dir + '/eeg_' + signal_name + '_15hz'  + '.bin'):
        return
    
    if draw:
        plt.figure(1)
        plt.suptitle("EEG - {}".format(patient_id))
    data_length = len(eeg)

    range = np.arange(0, data_length, stepsize)
    rangle_len = len(range)
    result_3hz = np.zeros((rangle_len,), dtype=np.float32)
    result_5hz = np.zeros((rangle_len,), dtype=np.float32)
    result_9hz = np.zeros((rangle_len,), dtype=np.float32)
    result_12hz = np.zeros((rangle_len,), dtype=np.float32)
    result_15hz = np.zeros((rangle_len,), dtype=np.float32)
    for i, v in enumerate(tqdm.tqdm(range)):
        from_i = max(0, v - halfwindow)
        to_i = min(data_length-1, v + halfwindow)
        if draw:
            plt.clf()
            plt.subplot(311)
            plt.plot(eeg[from_i : to_i], 'r-')
            plt.subplot(312)
        if do_plt_psd:
            psd_values, psd_freq = plt.psd(eeg[from_i : to_i], 512, Fs=200)
            result_3hz[i] = np.average(psd_values[0:8])
            result_5hz[i] = np.average(psd_values[8:16])
            result_9hz[i] = np.average(psd_values[16:24])
            result_12hz[i] = np.average(psd_values[24:32])
            result_15hz[i] = np.average(psd_values[32:40])
        if do_scikit_welch:
            f, Pxx = signal.welch(eeg[from_i : to_i], fs=200)
            if draw:
                plt.subplot(313)
                plt.semilogy(f, Pxx)

            result_3hz[i] = np.average(Pxx[0:8])
            result_5hz[i] = np.average(Pxx[8:16])
            result_9hz[i] = np.average(Pxx[16:24])
            result_12hz[i] = np.average(Pxx[24:32])
            result_15hz[i] = np.average(Pxx[32:40])
        if normalize:
            result_sum = result_3hz[i] + result_5hz[i] + result_9hz[i] + result_12hz[i] + result_15hz[i]
            if not np.isfinite(result_sum) or result_sum == 0:
                print("Warning: invalid eeg fft result sum:", result_sum)
                print("window: from-to-length: ", from_i, to_i, (to_i-from_i+1))
                print(result_3hz[i], result_5hz[i], result_9hz[i], result_12hz[i], result_15hz[i])
                result_sum = 1
            result_3hz[i] = result_3hz[i] / result_sum
            result_5hz[i] = result_5hz[i] / result_sum
            result_9hz[i] = result_9hz[i] / result_sum
            result_12hz[i] = result_12hz[i] / result_sum
            result_15hz[i] = result_15hz[i] / result_sum
            if result_sum < 0.01:
                print("really small sum: ", result_sum)
                print(result_3hz[i], result_5hz[i], result_9hz[i], result_12hz[i], result_15hz[i])
        
        if draw:
            plt.pause(0.1)
        if i % 1000 == 0 and draw:
            plt.clf()

    os.makedirs(output_dir, exist_ok=True)
    f = open(output_dir + '/eeg_' + signal_name + '_3hz'  + '.bin', 'w')
    result_3hz.tofile(f)
    f.close()
    f = open(output_dir + '/eeg_' + signal_name + '_5hz'  + '.bin', 'w')
    result_5hz.tofile(f)
    f.close()
    f = open(output_dir + '/eeg_' + signal_name + '_9hz'  + '.bin', 'w')
    result_9hz.tofile(f)
    f.close()
    f = open(output_dir + '/eeg_' + signal_name + '_12hz'  + '.bin', 'w')
    result_12hz.tofile(f)
    f.close()
    f = open(output_dir + '/eeg_' + signal_name + '_15hz'  + '.bin', 'w')
    result_15hz.tofile(f)
    f.close()
