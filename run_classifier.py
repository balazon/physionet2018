# - input: record_name
# - preprocesses input to generate python features
# - runs network on it
# - dumps .vec file

import sys
import os
import preprocess
import numpy as np

from train.train import Trainer

def classify_record(record_name):
    features_dir = "./features"
    preprocess.preprocess_record(record_name, '.', features_dir)

    predictions = run_classifier(features_dir)

    return predictions


# This function generates the predictions from a single model
def run_classifier(features_dir):

    tf_model = Trainer(data_dir = ".", run_name = "test_gpu_1", model = None)
    tf_model.data_loader.init_testing(features_dir)
    tf_model.init_infer()
    arousal_probabilities = tf_model.infer(features_dir)
    return arousal_probabilities

if __name__ == "__main__":
    record = sys.argv[1]
    out_dir = '.' # or 'annotations'?
    os.makedirs(out_dir, exist_ok=True)
    output_file = out_dir + os.path.sep + os.path.basename(record) + '.vec'
    results = classify_record(record)
    np.savetxt(output_file, results, fmt='%.3f')
    print("Saved {}".format(output_file))
    print("-size: {} bytes".format(os.path.getsize(output_file)))
