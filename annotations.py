import tqdm
import h5py
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy import stats

def print_attrs(name, obj):
    print(name)
    if 'iteritems' in obj.attrs:
        for key, val in obj.attrs.iteritems():
            print("    %s: %s" % (key, val))


# 0 data/sleep_stages/nonrem1
# 1 data/sleep_stages/nonrem2
# 2 data/sleep_stages/nonrem3
# 3 data/sleep_stages/rem
# 4 data/sleep_stages/wake
# 5 data/sleep_stages/undefined

#data/arousals
# +1: Designates arousal regions --->> 1
#  0: Designates non-arousal regions --->> 0
# -1: Designates regions that will not be scored --->> 2

def saveAnnotationAsBinaryFile(path, patientname, annotationsFile):
    draw = False
    skipIfExist = True
    features_dir = path.replace("training", "training_features")
    targetPath = features_dir + '/' + patientname + '.target'
    if skipIfExist and os.path.isfile(targetPath):
        return
    annotation_count = annotationsFile['data/arousals'].shape[0]
    target_count = annotation_count // 200
    target_arr = np.zeros((target_count, 2), dtype=np.int8)
    for i in tqdm.tqdm(range(target_count)):
        start_i = max(0, i * 200 - 100)
        stop_i = min(annotation_count, i * 200 + 100)

        # Warning! Arousal IDs are reorganized, because none of them should be negative, and invalid should be the highest.
        arousal_mode = stats.mode(annotationsFile['data/arousals'][start_i : stop_i])[0][0][0]
        target_arr[i, 0] = arousal_mode if arousal_mode >= 0 else 2

        # Warning! SleepStages indices are reorganized, because undefined should be last.
        target_arr[i,1] = np.argmax([np.sum(annotationsFile['data/sleep_stages/nonrem1'][0, start_i : stop_i]),
                                     np.sum(annotationsFile['data/sleep_stages/nonrem2'][0, start_i : stop_i]),
                                     np.sum(annotationsFile['data/sleep_stages/nonrem3'][0, start_i : stop_i]),
                                     np.sum(annotationsFile['data/sleep_stages/rem'][0, start_i : stop_i]),
                                     np.sum(annotationsFile['data/sleep_stages/wake'][0, start_i : stop_i]),
                                     np.sum(annotationsFile['data/sleep_stages/undefined'][0, start_i : stop_i]),
                                     ])
    
    if draw:
        plt.figure(1)
        plt.subplot(211)
        plt.ylabel('A / NA')
        plt.plot(target_arr[:,0])
        plt.subplot(212)
        plt.ylabel('Stages')
        plt.plot(target_arr[:,1])
        plt.show()
    f = open(targetPath, 'w')
    target_arr.tofile(f)
    f.close()
        
        

def processAnnotation(annotationsFile):
    #annotationsFile.visititems(print_attrs)
    plt.figure(1)

    plt.subplot(811)
    plt.ylabel('A/NA')
    plt.plot(annotationsFile['data/arousals'])

    plt.subplot(812)
    plt.ylabel('NR1')
    plt.plot(annotationsFile['data/sleep_stages/nonrem1'][0,:])

    plt.subplot(813)
    plt.ylabel('NR2')
    plt.plot(annotationsFile['data/sleep_stages/nonrem2'][0,:])

    plt.subplot(814)
    plt.ylabel('NR3')
    plt.plot(annotationsFile['data/sleep_stages/nonrem3'][0,:])

    plt.subplot(815)
    plt.ylabel('R')
    plt.plot(annotationsFile['data/sleep_stages/rem'][0,:])
    
    plt.subplot(816)
    plt.ylabel('W')
    plt.plot(annotationsFile['data/sleep_stages/wake'][0,:])
    
    plt.subplot(817)
    plt.ylabel('U')
    plt.plot(annotationsFile['data/sleep_stages/undefined'][0,:])

    plt.subplot(818)
    plt.ylabel('R+NRs+W+U')
    plt.plot(annotationsFile['data/sleep_stages/nonrem1'][0,:] + annotationsFile['data/sleep_stages/nonrem2'][0,:] +annotationsFile['data/sleep_stages/nonrem3'][0,:] +annotationsFile['data/sleep_stages/wake'][0,:]+annotationsFile['data/sleep_stages/undefined'][0,:]+annotationsFile['data/sleep_stages/rem'][0,:])

    plt.show()