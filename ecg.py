import matplotlib.pyplot as plt
import scipy.signal as signal
import scipy.fftpack
from scipy import stats
import numpy as np
from tqdm import tqdm as tqdm
import os
from scipy.ndimage.filters import maximum_filter1d
import time

plot_datas = []
def addPlot(data):
    plot_datas.append(data)

def clearPlots():
    plot_datas.clear()
    

def showPlots():
    num_plots = len(plot_datas)
    for idx, data in enumerate(plot_datas):
        plt.subplot(num_plots, 1, idx + 1)
        plt.plot(np.arange(0, len(data)), data, 'r-')
    plt.show()


# modified to use some percentage of a pooled_max value as a threshold at the end
def findPeaks(data, spacing=1, limit=None):
    """
    Janko Slavic peak detection algorithm and implementation.
    https://github.com/jankoslavic/py-tools/tree/master/findpeaks
    Finds peaks in `data` which are of `spacing` width and >=`limit`.
    :param ndarray data: data
    :param float spacing: minimum spacing to the next peak (should be 1 or more)
    :param float limit: peaks should have value greater or equal
    :return array: detected peaks indexes array
    """
    size = data.size
    x = np.zeros(size + 2 * spacing)
    x[:spacing] = data[0] - 1.e-6
    x[-spacing:] = data[-1] - 1.e-6
    x[spacing:spacing + size] = data
    peak_candidate = np.zeros(size)
    peak_candidate[:] = True
    for s in range(spacing):
        start = spacing - s - 1
        h_b = x[start: start + size]  # before
        start = spacing
        h_c = x[start: start + size]  # central
        start = spacing + s + 1
        h_a = x[start: start + size]  # after
        peak_candidate = np.logical_and(peak_candidate, np.logical_and(h_c > h_b, h_c > h_a))

    ind = np.argwhere(peak_candidate)
    ind = ind.reshape(ind.size)


    # max pooling with stride = window_size
    max_window = 1000
    rolling_max = np.zeros([int(np.ceil(len(data) / max_window))], dtype=np.float32)
    for i in range(len(rolling_max)):
        if i == len(data) // max_window:
            datasub = data[-max_window:]
        else:
            datasub = data[i * max_window : (i + 1) * max_window]
        rolling_max[i] = np.max(datasub)

    # if limit is not None:
    #         ind = ind[data[ind] > limit]
    
    ind = ind[data[ind] > (rolling_max[ind // max_window]) * 0.4]
    return ind



def getBPM(peak_idx, length):
    bpm_vals_at_peaks = 200 * 60 / np.gradient(peak_idx)
    bpm = np.interp(np.arange(length), peak_idx, bpm_vals_at_peaks)
    return bpm

def fft_plot(ecg):
    T = 1.0 / 200.0
    N = len(ecg)
    yf = scipy.fftpack.fft(ecg)
    xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
    fig, ax = plt.subplots()
    ax.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    plt.show()

def preprocess(data):
    # band pass filter
    # The power line noise is at 60Hz which can be filtered by filtering ~0.3 Hz with firwin (200 * 0.3 = 60)
    band_filter = signal.firwin(34, [0.001, 0.15], pass_zero=False, nyq=0.5)

    filtered = signal.filtfilt(band_filter, [1.0], data)
    return filtered

def processECG(ecg, name, output_path):
    # window = 5*200
    # center = np.random.randint(window, len(ecg) - window)
    # # center = 97743
    # # center = 496406
    # print("Center: ", center)
    # ecg = ecg[center - window: center + window]
    
    # fft_plot(ecg)

    filtered = preprocess(ecg)
    
    grad = np.gradient(filtered)
    myabs = np.abs(grad)
    
    moving_avg_window_ms = 80
    N = round(moving_avg_window_ms / 5.0)
    moving_avg = np.convolve(myabs, np.ones(N)/N, mode='same')
    moving_avg = np.convolve(moving_avg, np.ones(10)/10, mode='same')
    
    max_bpm = 180
    peak_min_spacing = int((60000 / max_bpm) / 5) #5 ms is the time between samples at 200 Hz
    peak_idx = findPeaks(moving_avg, peak_min_spacing)
    
    peaks = np.zeros([len(ecg)], dtype=np.float32)
    peaks[peak_idx] = 1.0
    
    # start = time.time()
    bpm = getBPM(peak_idx, len(ecg))
    # end = time.time()
    # print("getbpm : {} sec".format(end - start))
    downsample_factor = int(1.0 * 200) # resampled will have 1 second between samples

    resample_range = np.arange(0, len(bpm), downsample_factor)
    bpm_resamp = np.zeros([len(resample_range)], dtype=np.float32)
    resamp_window_half = 5000
    for idx, val in enumerate(resample_range):
        minIndex = max(0, val - resamp_window_half)
        maxIndex = min(len(bpm), val + resamp_window_half)
        bpm_windowed = bpm[minIndex : maxIndex]
        resamp_val = scipy.stats.trim_mean(bpm_windowed, 0.2)
        bpm_resamp[idx] = resamp_val

    norm_min = 40.0
    norm_max = 160.0
    norm_range = norm_max - norm_min
    
    bpm_normalized = (np.clip(bpm_resamp, norm_min, norm_max) - norm_min) / norm_range * 2.0 - 1.0


    # plt.figure(1)
    # plt.suptitle("ECG - {}".format(name))

    # clearPlots()
    # addPlot(ecg)
    # addPlot(filtered)
    # addPlot(grad)
    # addPlot(myabs)
    # addPlot(moving_avg)
    # addPlot(peaks)
    # addPlot(bpm)
    # addPlot(bpm_resamp)
    # addPlot(bpm_normalized)
    # showPlots()

    with open(output_path + '/ecg_bpm.bin', 'w') as file:
        bpm_normalized.tofile(file)


        